<%-- 
    Document   : openPayment
    Created on : Dec 26, 2017, 9:22:48 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Payment - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form id="myForm" action="openPayment" method="POST">
                    <div class="formContainer">
                        <h1>Open Payment</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">                            
                            <div class="form-group">
                                <label for="sourceAccount">Source Account:*</label>
                                <select class="inputBox" name="sourceAccount" id="sourceAccount" required>
                                    <option disabled selected value>Select Account</option>
                                    <c:forEach var="account" items="${bean.getAccountList()}">
                                        <option value="${account.getAccountNumber()}" id="${account.getAccountBalance()}">
                                            <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <p>Please select the <strong>type</strong> of payment.</p>
                            <div class="radio">
                                <label><input type="radio" name="paymentType" value="Bill" checked>Bill Payment</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="paymentType" value="Card">Card Settlement</label>
                            </div>
                            <div class="Bill payment">
                                <div class="form-group">
                                    <label for="profileType">Payment Profile:*</label>
                                    <select class="inputBox" name="profileType" id="profileType" required>
                                        <option disabled selected value>Select Profile</option>
                                        <c:forEach var="profile" items="${bean.getProfileList()}">
                                            <option value="${profile.getProfileID()}">
                                                <c:out value="${profile.getProfileName()} : ${profile.getProfileType()} : ${profile.getAccountNum()}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="Card payment" style="display:none">                                
                                <div class="form-group">
                                    <label for="cardSelect">Credit Card:*</label>
                                    <select class="inputBox" name="cardSelect" id="cardSelect" required disabled>
                                        <option disabled selected value>Select Card</option>
                                        <c:forEach var="profile" items="${bean.getCardList()}">
                                            <option value="${profile.getCardNumber()}">
                                                <c:out value="${profile.getCardType()} : ${profile.getCardNumber()}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="paymentAmount">Payment Amount:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" class="inputBox" name="paymentAmount" id="paymentAmount" placeholder="Enter Amount" required>
                                </div>
                            </div>
                            <div class="form-group alert alert-danger" id="transferError" style="display:none">
                                <strong>Error!</strong> Transfer amount cannot be negative.
                            </div>
                            <div class="form-group alert alert-danger" id="amtError" style="display:none">
                                <strong>Error!</strong> The source account does not have sufficient funds.
                            </div>
                            <c:if test="${not empty successMsg}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> Fund transfer complete.
                                </div>
                            </c:if>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Open Payment">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/openPayment.js"></script>
    </body>
</html>
