<%-- 
    Document   : changePassword
    Created on : Jan 22, 2018, 11:47:15 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="static/css/loginPage.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Change Password - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class ="CenterContainer">
            <div class="FormContainer">
                <form id="myForm" action="changePassword" method="POST">
                    <h1 id="welcomeText">Change Password</h1>
                    <h3 id="enterText">
                        Hey there, looks like you have forgotten your password. Please enter a new password below to reset your password.
                    </h3>
                    <div class="form-group">
                        <label id="formLabel">Enter Password :</label>
                        <input required name="password" id ="formInput" type="password" placeholder="Enter Password" class="password">
                    </div>
                    <div class="form-group">
                        <label id="formLabel">Re - Enter Password :</label>
                        <input required name="repassword" id ="formInput" type="password" placeholder="Enter Password" class="repassword">
                    </div>
                    <div class="form-group alert alert-danger" id="passwordError" style="display:none">
                        <center><strong>Error! </strong>Passwords should match!</center>
                    </div>
                    <div class="form-group">
                        <input type="submit" id="submitBtn" value="Reset">
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/changePassword.js"></script>
    </body>
</html>
