<%-- 
    Document   : viewUsers
    Created on : Jan 21, 2018, 10:22:14 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/viewBeneficiaries.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>View Users - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>View Users</h1>
                    <div class="cardListDiv">
                        <c:forEach var="user" items="${bean.getUserList()}">
                            <form action="viewUserAccount" method="POST">
                                <div class="card">                        
                                    <input type="hidden" name="userID" value="${user.getUserID()}">
                                    <label id="accName"><c:out value="${user.getFirstName()} ${user.getLastName()}"/></label><br>                    
                                    Email : <label><c:out value="${user.getUserEmail()}"/></label><br>                       
                                    Last Logged : <label><c:out value="${user.getLastLogin()}"/></label><br>
                                    <input type="submit" value="View User" id="viewBtn">
                                </div>
                            </form>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
