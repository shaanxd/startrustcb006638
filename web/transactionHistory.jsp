<%-- 
    Document   : transactionHistory
    Created on : Jan 4, 2018, 3:29:53 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <link rel="stylesheet" href="static/css/activityLog.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Transaction History - Startrust</title>
    </head>
    <body>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="transactionHistory" method="POST">
                    <div class="formContainer">
                        <h1>Transaction History</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fromDate">From:*</label>
                                        <input type="date" class="inputBox" name="fromDate" id="fromDate" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="toDate">To:*</label>
                                        <input type="date" class="inputBox" name="toDate" id="toDate" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="submitBtn" value="View Transactions ">
                            </div>
                            <div class="logContainer">
                                <c:forEach var="transaction" items="${transactionBean.getTransactionList()}">
                                    <div class= "activity">                    
                                        <label id="amount">Rs.<c:out value="${transaction.getTransferAmt()}"/></label><br> 
                                        From : <label id="userLabel"><c:out value="${transaction.getSourceNum()}"/></label><br>                            
                                        Destination : <label id="subjectLabel"><c:out value="${transaction.getDestinationNum()}"/></label><br>                             
                                        <label id="dateLabel"><c:out value="${transaction.getTransferType()} Transaction"/></label><br>                         
                                        Date : <label id="dateLabel"><c:out value="${transaction.getTransferDate()}"/></label><br>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
