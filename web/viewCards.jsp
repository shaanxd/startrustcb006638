<%-- 
    Document   : viewCards
    Created on : Jan 19, 2018, 3:24:55 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/viewCards.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>View Cards - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>View Cards</h1>
                    <div class="cardListDiv">
                        <c:forEach var="creditCard" items="${bean.getCardList()}">
                            <div class="card">                                
                                <label id="typeLabel"><c:out value="${creditCard.getCardType()} Card"/></label><br>
                                <span id=numLabel>CC Num: <label><c:out value="${creditCard.getCardNumber()}"/></label></span><br>                                   
                                Expires on : <label><c:out value="${creditCard.getExpiryDate()}"/></label><br>            
                                Debit Amount : <label><c:out value="Rs. ${creditCard.getDebitAmount()}"/></label><br>  
                                Credit  : <label><c:out value="Rs. ${creditCard.getCreditAmount()} of ${creditCard.getCreditLimit()}"/></label><br>
                                <div class="progress" style="height:30px">
                                    <div class="progress-bar progress-bar-striped active" id="pBar" role="progressbar"
                                    aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width:${creditCard.getPercentage()};height:30px;padding-top:5px">
                                        ${creditCard.getPercentage()}
                                    </div>
                                </div>
                            </div>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
