<%-- 
    Document   : managerDashboard
    Created on : Jan 21, 2018, 4:00:25 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/managerDashboard.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Dashboard - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Dashboard</h1>
                    <div class="divContainer">
                        <div class="row topDiv">
                            <div class="firstDiv">
                                <h4>Messages </h4>
                                <div class="messageDiv">
                                    <c:forEach var="message" items="${bean.getInboxList()}">
                                        <div class="singleDiv">                    
                                            <label id="accountBalance"><c:out value="${message.getSubject()}"/></label><br>                        
                                            <label><c:out value="${message.getOtherUser()}"/></label><br>                       
                                            <label><c:out value="${message.getDate()}"/></label><br>
                                        </div>
                                    </c:forEach>      
                                </div>
                            </div>
                            <div class="secondDiv">
                                <h4>Card Requests </h4>
                                <div class="cardRequestDiv">
                                    <c:forEach var="request" items="${bean.getRequestList()}">
                                        <div class="singleDiv">  
                                            <label id="accountBalance"><c:out value="${request.getCardType()} Card"/></label><br>                       
                                            Requested by: <label id="userLabel"><c:out value="${request.getUsername()}"/></label><br>                           
                                            On: <label id="dateLabel"><c:out value="${request.getRequestDate()}"/></label><br> 
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
