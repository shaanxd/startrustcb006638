<%-- 
    Document   : myAccounts
    Created on : Jan 20, 2018, 9:34:15 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/myAccounts.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>My Accounts - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>My Accounts</h1>
                    <div class="accountListDiv">
                        <c:forEach var="account" items="${bean.getAccountList()}">
                            <div class="accounts">       
                                <input type="hidden" name="accountID" value="${account.getAccountID()}">
                                <label id="accountType"><c:out value="${account.getAccountType()} account"/></label><br>                    
                                <label id="accountBalance"><c:out value="Rs.${account.getAccountBalance()}"/></label><br>                    
                                A/C Num : <label><c:out value="${account.getAccountNumber()}"/></label><br>                       
                                Alias : <label><c:out value="${account.getName()}"/></label><br>                    
                                Branch : <label><c:out value="${account.getBranchName()}"/></label><br>                    
                                Interest : <label><c:out value="${account.getInterestGained()}"/></label><br>
                            </div>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
