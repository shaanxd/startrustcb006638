<%-- 
    Document   : verifyInterest
    Created on : Jan 4, 2018, 2:32:32 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Verify Interest - Startrust</title>
    </head>
    <body>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="verifyInterest" method="POST">
                    <div class="formContainer">
                        <h1>Verify Interest</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <p>Please select the <strong>type</strong> of Account.</p>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Savings" checked required>Savings Account</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Fixed Deposit" required>Fixed-Deposit Account</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Current" required>Current Account</label>
                            </div>                         
                            <div class="form-group">
                                <label for="interestRate">Interest Rate:*</label>
                                <div class="input-group">
                                    <input type="number" class="inputBox" name="interestRate" id="interestRate" placeholder="Enter Interest" required>                                    
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Set Rate">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
