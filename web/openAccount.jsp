<%-- 
    Document   : openAccount
    Created on : Dec 27, 2017, 11:39:49 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Accounts - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form id="myForm" action="openAccount" method="post">
                    <div class="formContainer">
                        <h1>Request Account</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <div class="form-group">
                                <label for="accountType">Account Type:*</label>
                                <select class="inputBox" name="accountType" id="accountType" required>
                                    <option disabled selected value>Select Type</option>
                                    <option value="Savings">Savings Account</option>
                                    <option value="Current">Current Account</option>
                                    <option value="Fixed Deposit">Fixed-Deposit Account</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sourceAccount">Source Account:*</label>
                                <select class="inputBox" name="sourceAccount" id="sourceAccount" required>
                                    <option disabled selected value>Select Account</option>
                                    <c:forEach var="account" items="${accountList.getAccountList()}">
                                        <option value="${account.getAccountID()}" id="${account.getAccountBalance()}">
                                            <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>                           
                            <div class="form-group">
                                <label for="initialAmount">Initial Deposit*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" class="inputBox" name="initialAmount" id="initialAmount" placeholder="Enter Amount" required>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="accName">Account Name*</label>
                                <input type="text" class="inputBox" name="accName" id="accName" placeholder="Enter Name" required>
                            </div>  
                            <div class="form-group">
                                <label for="branchName">Desired Branch:*</label>
                                <select class="inputBox" name="branchName" id="branchName" required>
                                    <option disabled selected value>Select Branch</option>
                                    <option value="Wattala">Wattala</option>
                                    <option value="Kolpity">Kolpity</option>
                                    <option value="Dehiwala">Dehiwala</option>
                                    <option value="Kirulapone">Kirulapone</option>
                                </select>
                            </div>
                            <div class="form-group alert alert-danger" id="wrongAmount" style="display:none">
                                <strong>Invalid Amount!</strong> This account does not have sufficient funds.
                            </div>
                            <div class="form-group alert alert-danger" id="negAmount" style="display:none">
                                <strong>Invalid Amount!</strong> The amount cannot be negative.
                            </div>
                            <c:if test="${not empty successMessage}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> You have successfully requested an account.
                                </div>
                            </c:if>  
                            <p> Please note that an initial deposit is required to open an account and is 
                                deducted from the selected <strong>Source Account</strong>.</p>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Request Account">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/openAccount.js"></script>
    </body>
</html>
