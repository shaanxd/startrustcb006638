/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.list-unstyled.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('input[name="orderType"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);                    
        $(".order").not(targetBox).hide();
        $(targetBox).slideDown();
        if (inputValue===("beneficiary")){
            $(".creditCard :input,.payment :input").prop("disabled", true);
            $(".beneficiary :input").prop("disabled", false);
        }
        else if (inputValue===("payment")){
            $(".creditCard :input,.beneficiary :input").prop("disabled", true);                        
            $(".payment :input").prop("disabled", false);
        }
        else if (inputValue===("creditCard")){
            $(".creditCard :input").prop("disabled", false);                        
            $(".payment :input,.beneficiary :input").prop("disabled", true);
        }
    });
    $('#myForm').submit(function() {
        var accountBalance = parseFloat($('#sourceAccount option:selected').attr('id'));
        var orderAmount = parseFloat($('#orderAmount').val());
        if (accountBalance < orderAmount) {
            $('#amtError').fadeIn();
            $('#negError').hide();
            return false;
        }
        else if (orderAmount < 0)
        {
            $('#amtError').hide();
            $('#negError').fadeIn();
            return false;
        }
        else
        {
            return true;
        }
    });
});
document.getElementById('startFrom').valueAsDate = new Date();


