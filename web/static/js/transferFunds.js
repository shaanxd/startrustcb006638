/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('input[name="accountType"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);                    
        $(".account").not(targetBox).hide();
        $(targetBox).slideDown();
        if (inputValue===("Own")){
            $(".Startrust :input,.Other :input").prop("disabled", true);
            $(".Own :input").prop("disabled", false);
        }
        else if (inputValue===("Startrust")){
            $(".Own :input,.Other :input").prop("disabled", true);                        
            $(".Startrust :input").prop("disabled", false);
        }
        else if(inputValue===("Other")){
            $(".Own :input,.Startrust :input").prop("disabled", true);                        
            $(".Other :input").prop("disabled", false);
        }
    });
    $('#myForm').submit(function() {
        var paymentAmount = parseFloat($('#paymentAmount').val());
        var availableAmount = parseFloat($('#sourceAccount option:selected').attr('id'));
        var sourceAcc = $('#sourceAccount').val();
        var destAcc = $('#ownAccount').val();
        if (paymentAmount<0) {
            $('#transferError').fadeIn();
            $('#accError').hide();
            $('#amtError').hide();
            return false;
        }
        else if(paymentAmount > availableAmount){
            $('#transferError').hide();
            $('#accError').hide();
            $('#amtError').fadeIn();
            return false;
        }
        else if(sourceAcc===destAcc){
            $('#transferError').hide();
            $('#amtError').hide();
            $('#accError').fadeIn();
            return false;
        }
        else{
            return true;
        }
    });
});

