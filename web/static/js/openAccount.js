/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('#myForm').submit(function() {
        var accountBalance = parseFloat($('#sourceAccount option:selected').attr('id'));
        var initialAmount = parseFloat($('#initialAmount').val());
        if (accountBalance < initialAmount) {
            $('#negAmount').hide();
            $('#wrongAmount').fadeIn();
            return false;
        }
        else if(initialAmount < 0){
            $('#wrongAmount').hide();
            $('#negAmount').fadeIn();
            return false;
        }
        else
        {
            return true;
        }
    });
});

