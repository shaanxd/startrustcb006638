/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('.request').on('click',function(){
        $(".selectedRequestDiv").hide();
        $(this).addClass('active').siblings().removeClass('active');
        var user = $(this).children("#userLabel").text();
        var date = $(this).children("#dateLabel").text();
        var type = $(this).children("#typeLabel").text();                    
        var request = $(this).children("#requestLabel").text();   
        var creditLimit = $(this).children("#creditLabel").text();
        $("#requestID").val(request);
        $("#username").val(user);
        $("#cardType").val(type);
        $("#reqDate").val(date);
        $("#creditLimit").val(creditLimit);
        $("input:radio").removeAttr("checked");
        $(".radioBtn").removeClass('active');
        $(".selectedRequestDiv").fadeIn();
    });
    $('#approve').change(function () {
        if($(this).is(':checked')) {
            $('#cardNumber').attr('required');
            $('#expiryDate').attr('required');
        } 
    });
    $('#reject').change(function () {
        if($(this).is(':checked')) {
            $('#cardNumber').removeAttr('required');
            $('#expiryDate').removeAttr('required');
        }
    });
     $("#cardNumber").keyup(function () {
        $(this).val($(this).val().toCardFormat());
    });
});

String.prototype.toCardFormat = function () {
    return this.replace(/[^0-9]/g, "").substr(0, 16).split("").reduce(cardFormat, "");
    function cardFormat(str, l, i) {
        return str + ((!i || (i % 4)) ? "" : " ") + l;
    }
};


