/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('#myForm').submit(function() {
        var password = $('#password').val();
        var rePassword = $('#rePassword').val();
        if (password !== rePassword) {
            $('#passwordError').fadeIn();
            return false;
        }
        else
        {
            return true;
        }
    });
});


