<%-- 
    Document   : transferFunds
    Created on : Dec 27, 2017, 11:29:10 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Transfers - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form id="myForm" action="transferFunds" method="POST">
                    <div class="formContainer">
                        <h1>Transfer Funds</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <div class="form-group">
                                <label for="sourceAccount">Source Account:*</label>
                                <select class="inputBox" name="sourceAccount" id="sourceAccount" required>
                                    <option disabled selected value>Select Account</option>
                                    <c:forEach var="account" items="${bean.getAccountList()}">
                                        <option value="${account.getAccountNumber()}" id="${account.getAccountBalance()}">
                                            <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <p>Please select the <strong>type</strong> of account to transfer to.</p>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Own" checked>Personal Account</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Startrust">Startrust Beneficiary</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="accountType" value="Other">Other Beneficiary</label>
                            </div>
                            <div class="Own account">
                                <div class="form-group">
                                    <label for="ownAccount">Own Account*</label>
                                    <select class="inputBox" name="ownAccount" id="ownAccount" required>
                                        <option disabled selected value>Select Account</option>
                                        <c:forEach var="account" items="${bean.getAccountList()}">
                                            <option value="${account.getAccountNumber()}">
                                                <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="Startrust account" style="display:none">   
                                <div class="form-group">
                                    <label for="stBeneficiary">Startrust Beneficiary:*</label>
                                    <select class="inputBox" name="stBeneficiary" id="stBeneficiary" required disabled>
                                        <option disabled selected value>Select Account</option>
                                        <c:forEach var="account" items="${bean.getStList()}">
                                            <option value="${account.getAccountNumber()}">
                                                <c:out value="${account.getAccountName()} : ${account.getAccountNumber()}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div> 
                            </div>
                            <div class="Other account" style="display:none">   
                                <div class="form-group">
                                    <label for="otherBeneficiary">Other Beneficiary:*</label>
                                    <select class="inputBox" name="otherBeneficiary" id="otherBeneficiary" required disabled>
                                        <option disabled selected value>Select Beneficiary</option>
                                        <c:forEach var="account" items="${bean.getExtList()}">
                                            <option value="${account.getAccountNumber()}">
                                                <c:out value="${account.getAccountName()} : ${account.getAccountNumber()} : ${account.getAccountType()}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="paymentAmount">Payment Amount:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" class="inputBox" name="paymentAmount" id="paymentAmount" placeholder="Enter Amount" required>
                                </div>
                            </div>
                            <div class="form-group alert alert-danger" id="transferError" style="display:none">
                                <strong>Error!</strong> Transfer amount cannot be negative.
                            </div>
                            <div class="form-group alert alert-danger" id="amtError" style="display:none">
                                <strong>Error!</strong> The source account does not have sufficient funds.
                            </div>
                            <div class="form-group alert alert-danger" id="accError" style="display:none">
                                <strong>Error!</strong> Source and destination accounts cannot be the same.
                            </div>
                            <c:if test="${not empty successMsg}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> Fund transfer complete.
                                </div>
                            </c:if>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Transfer Funds">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/transferFunds.js"></script>
    </body>
</html>
