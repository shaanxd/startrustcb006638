<%-- 
    Document   : manageProfile
    Created on : Jan 3, 2018, 2:44:22 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/registerAccount.css">
        <link rel="stylesheet" href="static/css/manageProfile.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Manage Profile - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="manageProfile" method="post" id="myForm">
                    <div class="formContainer">                        
                        <h1>Manage Profile</h1>
                        <p>Please click the <strong>Edit Profile</strong> button to make changes to profile details.</p>
                        <c:if test="${not empty successMessage}">
                            <div class="form-group alert alert-success fade in">
                                <strong>Success!</strong> You have successfully edited your profile!
                            </div>
                        </c:if>
                        <h4 id="containerHeader">
                            Personal Details
                        </h4>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Username*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                                    
                                    <input type ="text" class="inputBox" placeholder="Username" name="username" value="${profileBean.getUserName()}" required disabled>
                                </div> 
                            </div>
                            <div class="col-sm-8 form-group">
                                <label class="control-label">Email*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type ="email" class="inputBox" placeholder="Email" name="emailAddress" value="${profileBean.getUserEmail()}" required disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">First Name*</label>
                                <input type ="text" class="inputBox" placeholder="First Name" name="firstName" value="${profileBean.getFirstName()}" required disabled>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Middle Name</label>
                                <input type ="text" class="inputBox" placeholder="Middle Name" name="middleName" value="${profileBean.getMiddleName()}" disabled>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Last Name*</label>
                                <input type ="text" class="inputBox" placeholder="Last Name" name="lastName" value="${profileBean.getLastName()}" required disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">N.I.C*</label>                                
                                <div class="input-group">
                                    <input type ="text" class="inputBox" placeholder="N.I.C" name="identityNumber" value="${profileBean.getIdentityNum()}" required disabled>
                                    <span class="input-group-addon">v</span>
                                </div>                                
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Passport I.D</label>
                                <input type ="text" class="inputBox" placeholder="Passport I.D" name="passportNumber" value="${profileBean.getPassportNum()}" disabled>
                            </div>
                        </div>
                        <hr id="horizontalLine">
                        <div class="profileImgContainer">
                            <center>
                                <img id="profileImg" src="static/img/avatar.png">
                            </center>
                            <br>
                             <label class="btn fileBtn">
                                Browse <input class="editInput" type="file" style="display: none;" name="profileImg" accept="image/*">
                            </label>
                        </div>
                        <hr id="horizontalLine">
                        <h4 id="containerHeader">
                            Contact Details
                        </h4>
                        <div class="row">
                            <div class="col-sm-8 form-group">
                                <label class="control-label">Address*</label>
                                <input type ="text" class="inputBox editInput" placeholder="Address" name="address" value="${profileBean.getUserAddress()}" required>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">City*</label>
                                <input type ="text" class="inputBox editInput" placeholder="City" name="city" value="${profileBean.getUserCity()}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">ZIP Code</label>
                                <input type ="text" class="inputBox editInput" placeholder="ZIP Code" name="zipCode" value="${profileBean.getZipCode()}">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Mobile No.*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                    <input type ="text" class="inputBox editInput" placeholder="Mobile No." name="mobileNumber" value="${profileBean.getMobileNum()}" required>
                                </div>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Home No.</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    <input type ="text" class="inputBox editInput" placeholder="Home No." name="homeNumber" value="${profileBean.getHomeNum()}">
                                </div>
                            </div>
                        </div>
                        <hr id="horizontalLine">
                        <h4 id="containerHeader">
                            Change Password
                        </h4><div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Password*</label>                                                                    
                                <input type ="password" class="inputBox editInput" placeholder="Password" name="password" id="password">
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Renter Password*</label>                                                                    
                                <input type ="password" class="inputBox editInput" placeholder="Re-Enter Password" name="rePassword" id="rePassword" >
                            </div>
                        </div>
                        <div class="form-group alert alert-danger fade in" id="passwordError" style="display:none">
                                <strong>Wrong Password!</strong> Please enter identical Passwords in both text fields.
                        </div>
                        <div class="row" id="btnSet">
                            <div class="col-sm-12" form-group>
                                <input class="btnSet" type ="submit" id="submitBtn" value="Edit Profile">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/manageProfile.js"></script>
    </body>
</html>
