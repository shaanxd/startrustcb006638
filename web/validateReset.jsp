<%-- 
    Document   : validateReset
    Created on : Jan 22, 2018, 10:20:26 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="static/css/loginPage.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Reset Password - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class ="CenterContainer">
            <div class="FormContainer">
                <form action="validateReset" method="POST">
                    <h1 id="welcomeText">Reset Password</h1>
                    <h3 id="enterText">
                        A temporary password has been sent to your email address. Please enter it in the below text field.
                    </h3>
                    <div class="form-group">
                        <label id="formLabel">Enter Password :</label>
                        <input required name="password" id ="formInput" type="password" placeholder="Enter Password">
                    </div>
                    <c:if test="${not empty errorMessage}">
                        <div class="form-group alert alert-danger" id="passwordError">
                            <center><strong>Error! </strong>Wrong code!</center>
                        </div>
                    </c:if>                    
                    <div class="form-group">
                        <input type="submit" id="submitBtn" value="Validate">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
