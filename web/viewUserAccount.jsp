<%-- 
    Document   : viewUserAccount
    Created on : Jan 21, 2018, 10:48:19 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/registerAccount.css">
        <link rel="stylesheet" href="static/css/viewUserAccount.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Manage Profile - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="manageProfile" method="post" id="myForm">
                    <div class="formContainer">        
                        <div class="details">                
                            <h1>View User</h1>
                            <div class="detailsDiv">
                                <h4 id="containerHeader">
                                    Personal Details
                                </h4>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Username*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                                    
                                            <input type ="text" class="inputBox" placeholder="Username" name="username" value="${bean.getUserName()}" readonly>
                                        </div> 
                                    </div>
                                    <div class="col-sm-8 form-group">
                                        <label class="control-label">Email*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type ="email" class="inputBox" placeholder="Email" name="emailAddress" value="${bean.getUserEmail()}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">First Name*</label>
                                        <input type ="text" class="inputBox" placeholder="First Name" name="firstName" value="${bean.getFirstName()}" readonly>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Middle Name</label>
                                        <input type ="text" class="inputBox" placeholder="Middle Name" name="middleName" value="${bean.getMiddleName()}" readonly>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Last Name*</label>
                                        <input type ="text" class="inputBox" placeholder="Last Name" name="lastName" value="${bean.getLastName()}" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">N.I.C*</label>                                
                                        <div class="input-group">
                                            <input type ="text" class="inputBox" placeholder="N.I.C" name="identityNumber" value="${bean.getIdentityNum()}" readonly>
                                            <span class="input-group-addon">v</span>
                                        </div>                                
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Passport I.D</label>
                                        <input type ="text" class="inputBox" placeholder="Passport I.D" name="passportNumber" value="${bean.getPassportNum()}" readonly>
                                    </div>
                                </div>
                                <h4 id="containerHeader">
                                    Contact Details
                                </h4>
                                <div class="row">
                                    <div class="col-sm-8 form-group">
                                        <label class="control-label">Address*</label>
                                        <input type ="text" class="inputBox editInput" placeholder="Address" name="address" value="${bean.getUserAddress()}" readonly>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">City*</label>
                                        <input type ="text" class="inputBox editInput" placeholder="City" name="city" value="${bean.getUserCity()}" readonly>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">ZIP Code</label>
                                        <input type ="text" class="inputBox editInput" placeholder="ZIP Code" name="zipCode" value="${bean.getZipCode()}" readonly>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Mobile No.*</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                            <input type ="text" class="inputBox editInput" placeholder="Mobile No." name="mobileNumber" value="${bean.getMobileNum()}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 form-group">
                                        <label class="control-label">Home No.</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                            <input type ="text" class="inputBox editInput" placeholder="Home No." name="homeNumber" value="${bean.getHomeNum()}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr id="horizontalLine">
                        <div class="listDiv">
                            <div class="col-sm-6 transactionDiv">
                                <h4 id="containerHeader">
                                    User Transactions
                                </h4>
                                <div class="transactionList">
                                    <c:forEach var="transaction" items="${bean.getTransactionList()}">
                                        <div class= "activity">                    
                                            <label id="amount">Rs.<c:out value="${transaction.getTransferAmt()}"/></label><br> 
                                            From : <label id="userLabel"><c:out value="${transaction.getSourceNum()}"/></label><br>                            
                                            Destination : <label id="subjectLabel"><c:out value="${transaction.getDestinationNum()}"/></label><br>                             
                                            <label id="dateLabel"><c:out value="${transaction.getTransferType()} Transaction"/></label><br>                         
                                            Date : <label id="dateLabel"><c:out value="${transaction.getTransferDate()}"/></label><br>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="col-sm-6 accountsDiv">
                                <h4 id="containerHeader">
                                    User Accounts
                                </h4>
                                <div class="accountList">
                                    <c:forEach var="account" items="${bean.getAccountList()}">
                                    <div class="accounts">       
                                        <input type="hidden" name="accountID" value="${account.getAccountID()}">
                                        <label id="accountType"><c:out value="${account.getAccountType()} account"/></label><br>                    
                                        <label id="accountBalance"><c:out value="Rs.${account.getAccountBalance()}"/></label><br>                    
                                        A/C Num : <label><c:out value="${account.getAccountNumber()}"/></label><br>                       
                                        Alias : <label><c:out value="${account.getName()}"/></label><br>                    
                                        Branch : <label><c:out value="${account.getBranchName()}"/></label><br>                    
                                        Interest : <label><c:out value="${account.getInterestGained()}"/></label><br>
                                    </div>
                                </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
