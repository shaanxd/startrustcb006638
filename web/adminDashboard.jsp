<%-- 
    Document   : adminDashboard
    Created on : Jan 21, 2018, 2:51:00 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/adminDashboard.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Dashboard - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Dashboard</h1>
                    <div class="divContainer">
                        <div class="row topDiv">
                            <div class="firstDiv">
                                <h4>Messages </h4>
                                <div class="messageDiv">
                                    <c:forEach var="message" items="${bean.getInboxList()}">
                                        <div class="singleDiv">                    
                                            <label id="accountBalance"><c:out value="${message.getSubject()}"/></label><br>                        
                                            <label><c:out value="${message.getOtherUser()}"/></label><br>                       
                                            <label><c:out value="${message.getDate()}"/></label><br>
                                        </div>
                                    </c:forEach>      
                                </div>
                            </div>
                            <div class="secondDiv">
                                <h4>Interest Rates </h4>
                                <div class="col-sm-4 interestDiv">
                                    <div class="savingsDiv">
                                        <h3>Savings</h3>                        
                                            <label id="interestAmount"><c:out value="${bean.getSavingsRate()}%"/></label><br>
                                    </div>
                                </div>
                                <div class="col-sm-4 interestDiv">
                                    <div class="savingsDiv">
                                        <h3>Current</h3>                              
                                            <label id="interestAmount"><c:out value="${bean.getCurrentRate()}%"/></label><br>                    
                                    </div>
                                </div>
                                <div class="col-sm-4 interestDiv">
                                    <div class="savingsDiv">
                                        <h3>Fixed-Deposit</h3>                    
                                        <label id="interestAmount"><c:out value="${bean.getFdRate()}%"/></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
