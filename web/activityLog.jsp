<%-- 
    Document   : activityLog
    Created on : Jan 4, 2018, 2:51:54 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <link rel="stylesheet" href="static/css/activityLog.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Activity Log - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="activityLog" method="POST">
                    <div class="formContainer">
                        <h1>Activity Log</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <div class="form-group">
                                <label for="userAccount">Select User*</label>
                                <select class="inputBox" name="userAccount" id="userAccount" required>
                                    <option disabled selected value>Select User</option>
                                    <c:forEach var="user" items="${bean.getUserList()}">
                                        <option value="${user.getUserID()}">
                                            <c:out value="${user.getUserName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" id="submitBtn" value="View Log ">
                            </div>
                            <div class="logContainer">
                                <c:forEach var="activity" items="${activityList.getActivityList()}">
                                    <div class= "activity">
                                        <label id="amount"><c:out value="${activity.getActivityType()}"/></label><br>                            
                                        <label id="subjectLabel"><c:out value="${activity.getActivityDescription()}"/></label><br>                         
                                        On: <label id="dateLabel"><c:out value="${activity.getDateTime()}"/></label><br>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/userApprovals.js"></script>
    </body>
</html>
