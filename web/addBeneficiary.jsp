<%-- 
    Document   : addBeneficiary
    Created on : Dec 25, 2017, 10:03:56 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/addBSO.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Add Beneficiary - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Add Beneficiaries</h1>
                    <p>Please note that the fields marked with * are <strong>Mandatory</strong></p>
                    <div class="formDiv">
                        <form id="myForm" action="addBeneficiary" method="POST">
                            <div class="form-group">
                                <label for="beneficiaryName">Beneficiary Name:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" class="inputBox" name="beneficiaryName" id="beneficiaryName" placeholder="Enter Name" required>
                                </div> 
                            </div>                            
                            <p>Please select the <strong>type</strong> of beneficiary.</p>
                            <div class="radio">
                                <label><input type="radio" name="beneficiaryType" value="Startrust" checked>Startrust Beneficiary</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="beneficiaryType" value="External">External Beneficiary</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="beneficiaryType" value="Mobile">Mobile Beneficiary</label>
                            </div>
                            <div class="form-group">
                                <label for="beneficiaryNumber">Beneficiary No:*</label><br>
                                <small>Please enter mobile number if type is 'Mobile'</small>
                                <input type="text" class="inputBox" name="beneficiaryNumber" id="beneficiaryNumber" placeholder="Account No" required>
                            </div>
                            <div class="External Beneficiary" style="display:none">
                                <div class="form-group">
                                    <label for="beneficiaryBank">Beneficiary Bank:*</label>
                                    <input type="text" class="inputBox" name="beneficiaryBank" id="beneficiaryBank" placeholder="Bank Name" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="beneficiaryBranch">Beneficiary Branch:*</label>
                                    <input type="text" class="inputBox" name="beneficiaryBranch" id="beneficiaryBranch" placeholder="Branch Name" required disabled>
                                </div>
                            </div>    
                            <c:if test="${not empty noAcc}">
                                <div class="form-group alert alert-danger" id="successMessage">
                                    <strong>Error!</strong> The Startrust account you entered is not present.
                                </div>
                            </c:if>  
                            <c:if test="${not empty yesAcc}">
                                <div class="form-group alert alert-danger" id="successMessage">
                                    <strong>Error!</strong> The beneficiary account is already present in your profile.
                                </div>
                            </c:if>  
                            <c:if test="${not empty successMsg}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> You have successfully requested an account.
                                </div>
                            </c:if>  
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Add Beneficiary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script> 
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.list-unstyled.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                $('input[name="beneficiaryType"]').click(function(){
                    var inputValue = $(this).attr("value");
                    var targetBox = $("." + inputValue);                    
                    $(".Beneficiary").not(targetBox).slideUp();
                    $(targetBox).slideDown();
                    if (inputValue===("Startrust")){
                        $(".External :input").prop("disabled", true);
                    }
                    else if (inputValue===("External")){                     
                        $(".External :input").prop("disabled", false);
                    }
                    else if (inputValue===("Mobile")){
                        $(".External :input").prop("disabled", true);
                    }
                });                
            }); 
        </script>
    </body>
</html>
