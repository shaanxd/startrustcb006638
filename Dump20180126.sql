CREATE DATABASE  IF NOT EXISTS `wmadassignment` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wmadassignment`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: wmadassignment
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accountrequest`
--

DROP TABLE IF EXISTS `accountrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountrequest` (
  `requestID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `requestDate` date NOT NULL,
  `accountType` varchar(50) NOT NULL,
  `sourceAccountID` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `initialDeposit` double NOT NULL,
  `branchName` varchar(50) NOT NULL,
  PRIMARY KEY (`requestID`),
  KEY `userID_idx` (`userID`),
  KEY `sourceAccountID_idx` (`sourceAccountID`),
  CONSTRAINT `sourceAccountID` FOREIGN KEY (`sourceAccountID`) REFERENCES `bankaccount` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountrequest`
--

LOCK TABLES `accountrequest` WRITE;
/*!40000 ALTER TABLE `accountrequest` DISABLE KEYS */;
INSERT INTO `accountrequest` VALUES (27,8,'2018-01-21','Savings',4,'Bahaa Account',1223,'Kolpity'),(28,8,'2018-01-21','Fixed Deposit',4,'Puhuu Account',4324,'Dehiwala'),(29,1,'2018-01-22','Fixed Deposit',5,'College Payments',150000,'Kolpity');
/*!40000 ALTER TABLE `accountrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounttype`
--

DROP TABLE IF EXISTS `accounttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounttype` (
  `typeID` int(11) NOT NULL AUTO_INCREMENT,
  `accountType` varchar(50) NOT NULL,
  `interestRate` double NOT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounttype`
--

LOCK TABLES `accounttype` WRITE;
/*!40000 ALTER TABLE `accounttype` DISABLE KEYS */;
INSERT INTO `accounttype` VALUES (1,'Savings',2),(2,'Current',3),(3,'Fixed Deposit',10);
/*!40000 ALTER TABLE `accounttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activitylog`
--

DROP TABLE IF EXISTS `activitylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activitylog` (
  `activityID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `activityType` varchar(50) NOT NULL,
  `activityDescription` varchar(255) NOT NULL,
  `activityDate` datetime NOT NULL,
  PRIMARY KEY (`activityID`),
  KEY `activityOwnerID_idx` (`userID`),
  CONSTRAINT `activityOwnerID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activitylog`
--

LOCK TABLES `activitylog` WRITE;
/*!40000 ALTER TABLE `activitylog` DISABLE KEYS */;
INSERT INTO `activitylog` VALUES (1,1,'Payment Profile','Added a payment profile to the portal','2018-01-12 16:10:38'),(2,1,'Account Request','Requested a bank account from the portal','2018-01-14 11:04:41'),(3,1,'Payment Profile','Added a payment profile to the portal','2018-01-15 17:14:29'),(4,1,'Account Request','Requested a bank account from the portal','2018-01-15 20:09:51'),(5,8,'Account Request','Requested a bank account from the portal','2018-01-15 20:10:09'),(6,1,'Account Request','Requested a bank account from the portal','2018-01-15 22:11:24'),(7,8,'Account Request','Requested a bank account from the portal','2018-01-15 22:12:00'),(8,1,'Account Request','Requested a bank account from the portal','2018-01-15 23:28:16'),(9,8,'Account Request','Requested a bank account from the portal','2018-01-15 23:28:39'),(10,1,'Account Request','Requested a bank account from the portal','2018-01-16 08:58:52'),(11,8,'Account Request','Requested a bank account from the portal','2018-01-16 09:00:22'),(12,1,'Account Request','Requested a bank account from the portal','2018-01-16 10:53:04'),(13,8,'Account Request','Requested a bank account from the portal','2018-01-16 10:53:26'),(14,1,'Account Request','Requested a bank account from the portal','2018-01-16 11:03:54'),(15,8,'Account Request','Requested a bank account from the portal','2018-01-16 11:04:09'),(16,1,'Card Request','Requested a Gold card from the portal','2018-01-16 13:21:59'),(17,1,'Account Request','Requested a bank account from the portal','2018-01-16 13:36:00'),(18,8,'Card Request','Requested a Platinum card from the portal','2018-01-16 00:00:00'),(19,1,'Card Request','Requested a Gold card from the portal','2018-01-16 00:00:00'),(20,8,'Card Request','Requested a Diamond card from the portal','2018-01-16 00:00:00'),(21,8,'Payment Profile','Added a payment profile to the portal','2018-01-19 23:19:31'),(22,8,'Card Request','Requested a Diamond card from the portal','2018-01-20 00:00:00'),(23,1,'Account Request','Requested a bank account from the portal','2018-01-20 20:07:37'),(24,1,'Account Request','Requested a bank account from the portal','2018-01-20 20:11:01'),(25,1,'Payment Profile','Added a payment profile to the portal','2018-01-21 01:01:06'),(26,1,'Payment Profile','Added a payment profile to the portal','2018-01-21 01:13:20'),(27,1,'Card Request','Requested a Platinum card from the portal','2018-01-21 00:00:00'),(28,1,'Card Request','Requested a Diamond card from the portal','2018-01-21 00:00:00'),(29,8,'Card Request','Requested a Gold card from the portal','2018-01-21 00:00:00'),(30,8,'Card Request','Requested a Platinum card from the portal','2018-01-21 00:00:00'),(31,1,'Payment Profile','Added a payment profile to the portal','2018-01-21 11:55:12'),(32,1,'Account Request','Requested a bank account from the portal','2018-01-21 12:33:38'),(33,8,'Account Request','Requested a bank account from the portal','2018-01-21 12:34:10'),(34,1,'Card Request','Requested a Platinum card from the portal','2018-01-21 00:00:00'),(35,1,'Account Request','Requested a bank account from the portal','2018-01-21 12:49:15'),(36,8,'Account Request','Requested a bank account from the portal','2018-01-21 12:49:35'),(37,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:11:18'),(38,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:11:26'),(39,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:12:48'),(40,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:13:52'),(41,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:13:59'),(42,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:14:07'),(43,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:20:16'),(44,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:26:31'),(45,1,'Account Request','Requested a bank account from the portal','2018-01-21 13:26:50'),(46,8,'Account Request','Requested a bank account from the portal','2018-01-21 13:27:19'),(47,8,'Account Request','Requested a bank account from the portal','2018-01-21 13:27:33'),(48,1,'Card Request','Requested a Gold card from the portal','2018-01-21 00:00:00'),(49,1,'Card Request','Requested a Diamond card from the portal','2018-01-21 00:00:00'),(50,8,'Card Request','Requested a Gold card from the portal','2018-01-21 00:00:00'),(51,8,'Card Request','Requested a Gold card from the portal','2018-01-21 00:00:00'),(52,1,'Account Request','Requested a bank account from the portal','2018-01-22 19:30:54'),(53,1,'Logout','Logged out Successfully.','2018-01-24 20:32:46'),(54,1,'Login','Logged in Successfully.','2018-01-24 20:32:59'),(55,1,'Logout','Logged out Successfully.','2018-01-24 20:33:08'),(56,8,'Login','Logged in Successfully.','2018-01-24 20:33:35'),(57,8,'Logout','Logged out Successfully.','2018-01-24 20:33:41'),(58,8,'Login','Logged in Successfully.','2018-01-24 20:36:03'),(59,8,'Update Profile','Successfully updated profile details','2018-01-24 20:36:18'),(60,8,'Payment Profile','Added a payment profile to the portal','2018-01-24 20:37:15'),(61,8,'Logout','Logged out Successfully.','2018-01-24 20:38:21'),(62,1,'Login','Logged in Successfully.','2018-01-24 21:15:25'),(63,1,'Logout','Logged out Successfully.','2018-01-24 21:16:07'),(64,1,'Login','Logged in Successfully.','2018-01-24 21:19:03'),(65,1,'Login','Logged in Successfully.','2018-01-24 22:56:08'),(66,1,'Logout','Logged out Successfully.','2018-01-24 22:56:10'),(67,1,'Login','Logged in Successfully.','2018-01-25 08:56:14'),(68,1,'Logout','Logged out Successfully.','2018-01-25 08:56:29'),(69,1,'Login','Logged in Successfully.','2018-01-25 11:11:39'),(70,1,'Logout','Logged out Successfully.','2018-01-25 11:23:48'),(71,1,'Login','Logged in Successfully.','2018-01-25 11:24:01'),(72,1,'Logout','Logged out Successfully.','2018-01-25 11:47:27'),(73,1,'Login','Logged in Successfully.','2018-01-25 12:07:48'),(74,1,'Logout','Logged out Successfully.','2018-01-25 12:07:52'),(75,1,'Login','Logged in Successfully.','2018-01-25 12:16:56'),(76,1,'Logout','Logged out Successfully.','2018-01-25 12:17:07'),(77,1,'Login','Logged in Successfully.','2018-01-25 21:20:34'),(78,1,'Logout','Logged out Successfully.','2018-01-25 21:21:25'),(79,8,'Login','Logged in Successfully.','2018-01-25 21:21:29'),(80,8,'Logout','Logged out Successfully.','2018-01-25 21:21:31'),(81,1,'Login','Logged in Successfully.','2018-01-25 21:21:35'),(82,1,'Logout','Logged out Successfully.','2018-01-25 21:21:41'),(83,8,'Login','Logged in Successfully.','2018-01-25 21:21:45'),(84,8,'Logout','Logged out Successfully.','2018-01-25 21:21:46'),(85,1,'Login','Logged in Successfully.','2018-01-25 21:21:51'),(86,1,'Logout','Logged out Successfully.','2018-01-25 21:22:20'),(87,8,'Login','Logged in Successfully.','2018-01-25 21:22:24'),(88,8,'Logout','Logged out Successfully.','2018-01-25 21:22:56'),(89,1,'Login','Logged in Successfully.','2018-01-25 22:08:24'),(90,1,'Logout','Logged out Successfully.','2018-01-25 22:09:09'),(91,1,'Login','Logged in Successfully.','2018-01-26 03:50:25'),(92,1,'Payment Profile','Added a payment profile to the portal','2018-01-26 04:17:40'),(93,1,'Login','Logged in successfully','2018-01-26 04:22:42'),(94,1,'Logout','Logged out successfully','2018-01-26 04:23:10'),(95,1,'Login','Logged in successfully','2018-01-26 04:23:16'),(96,1,'Logout','Logged out successfully','2018-01-26 04:23:23'),(97,1,'Login','Logged in successfully','2018-01-26 04:24:15'),(98,1,'Updated Profile','Updated profile details','2018-01-26 04:24:20'),(99,1,'Updated Profile','Updated profile details','2018-01-26 04:24:25'),(100,1,'Logout','Logged out successfully','2018-01-26 04:24:26'),(101,1,'Login','Logged in successfully','2018-01-26 04:25:30'),(102,1,'Card Request','Requested a Diamond card from the portal','2018-01-26 00:00:00'),(103,1,'Logout','Logged out successfully','2018-01-26 04:25:54'),(104,1,'Login','Logged in successfully','2018-01-26 04:26:35'),(105,1,'Logout','Logged out successfully','2018-01-26 04:27:29'),(106,8,'Login','Logged in successfully','2018-01-26 04:27:35'),(107,8,'Logout','Logged out successfully','2018-01-26 04:27:40'),(108,1,'Login','Logged in successfully','2018-01-26 04:27:43'),(109,1,'Logout','Logged out successfully','2018-01-26 04:28:05'),(110,1,'Login','Logged in successfully','2018-01-26 04:28:08'),(111,1,'Logout','Logged out successfully','2018-01-26 04:28:14'),(112,8,'Login','Logged in successfully','2018-01-26 04:28:18'),(113,8,'Logout','Logged out successfully','2018-01-26 04:28:34'),(114,1,'Login','Logged in successfully','2018-01-26 04:28:40'),(115,1,'Logout','Logged out successfully','2018-01-26 04:29:04'),(116,1,'Logout','Logged out successfully','2018-01-26 04:33:55'),(117,1,'Login','Logged in successfully','2018-01-26 04:34:01'),(118,1,'Updated Profile','Updated profile details','2018-01-26 04:34:09'),(119,1,'Logout','Logged out successfully','2018-01-26 04:34:15'),(120,1,'Login','Logged in successfully','2018-01-26 04:34:47'),(121,1,'Logout','Logged out successfully','2018-01-26 04:35:12'),(122,1,'Logout','Logged out successfully','2018-01-26 10:22:19'),(123,1,'Login','Logged in successfully','2018-01-26 12:12:55'),(124,1,'Logout','Logged out successfully','2018-01-26 12:13:07'),(125,1,'Login','Logged in successfully','2018-01-26 13:08:19'),(126,1,'Logout','Logged out successfully','2018-01-26 13:08:27'),(127,1,'Login','Logged in successfully','2018-01-26 13:08:50'),(128,1,'Logout','Logged out successfully','2018-01-26 13:09:07'),(129,9,'Login','Logged in successfully','2018-01-26 13:09:12'),(130,9,'Logout','Logged out successfully','2018-01-26 13:09:57'),(131,1,'Login','Logged in successfully','2018-01-26 13:10:01'),(132,1,'Logout','Logged out successfully','2018-01-26 13:10:09'),(133,9,'Login','Logged in successfully','2018-01-26 13:10:15'),(134,9,'Logout','Logged out successfully','2018-01-26 13:11:32'),(135,1,'Login','Logged in successfully','2018-01-26 13:11:35'),(136,1,'Logout','Logged out successfully','2018-01-26 13:11:37'),(137,9,'Login','Logged in successfully','2018-01-26 13:11:42'),(138,9,'Logout','Logged out successfully','2018-01-26 13:11:57'),(139,1,'Login','Logged in successfully','2018-01-26 13:12:00'),(140,1,'Logout','Logged out successfully','2018-01-26 13:13:07'),(141,1,'Login','Logged in successfully','2018-01-26 13:19:51'),(142,1,'Logout','Logged out successfully','2018-01-26 13:20:30');
/*!40000 ALTER TABLE `activitylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bankaccount`
--

DROP TABLE IF EXISTS `bankaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankaccount` (
  `accountID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `accountNumber` varchar(50) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `accountType` varchar(50) NOT NULL,
  `branchName` varchar(50) NOT NULL,
  `balanceAmount` double NOT NULL,
  `interestGained` double DEFAULT NULL,
  PRIMARY KEY (`accountID`),
  UNIQUE KEY `accountNumber_UNIQUE` (`accountNumber`),
  KEY `ownerID_idx` (`userID`),
  CONSTRAINT `ownerID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankaccount`
--

LOCK TABLES `bankaccount` WRITE;
/*!40000 ALTER TABLE `bankaccount` DISABLE KEYS */;
INSERT INTO `bankaccount` VALUES (1,1,'2354678','Personal Account','Savings','Wattala',271843,0),(4,8,'786124','Personal Expenses','Current','Dehiwala',257034,0),(5,1,'2354454','Salary Account','Savings','Wattala',164478,0),(19,1,'3534536645','Rent Account','Fixed Deposit','Kolpity',60000,NULL),(20,1,'98765','Tomato Account','Current','Kirulapone',10000,NULL),(21,9,'1246546','APIIT Account','Current','Dehiwala',450000,NULL);
/*!40000 ALTER TABLE `bankaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficiaryprofile`
--

DROP TABLE IF EXISTS `beneficiaryprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beneficiaryprofile` (
  `benID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `benType` varchar(50) NOT NULL,
  `benName` varchar(50) NOT NULL,
  `benNumber` varchar(50) NOT NULL,
  `benBank` varchar(50) DEFAULT NULL,
  `benBranch` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`benID`),
  KEY `benProfileUserID_idx` (`userID`),
  CONSTRAINT `benProfileUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficiaryprofile`
--

LOCK TABLES `beneficiaryprofile` WRITE;
/*!40000 ALTER TABLE `beneficiaryprofile` DISABLE KEYS */;
INSERT INTO `beneficiaryprofile` VALUES (1,1,'Mobile','Dada Mobile','0777776772',NULL,NULL),(2,1,'External','Mom Sampath','672131476','Sampath','Wattala'),(3,1,'Startrust','Hanau','786124','Startrust','Dehiwala'),(5,1,'Mobile','My Dialog','0774406047',NULL,NULL),(6,9,'Startrust','Sayid','3534536645','Startrust','Kolpity');
/*!40000 ALTER TABLE `beneficiaryprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardrequest`
--

DROP TABLE IF EXISTS `cardrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardrequest` (
  `requestID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `accountID` int(11) NOT NULL,
  `cardType` varchar(50) NOT NULL,
  `creditLimit` double NOT NULL,
  `requestDate` date NOT NULL,
  PRIMARY KEY (`requestID`),
  KEY `cardRequestUserID_idx` (`userID`),
  KEY `cardRequestAccountID_idx` (`accountID`),
  CONSTRAINT `cardRequestAccountID` FOREIGN KEY (`accountID`) REFERENCES `bankaccount` (`accountID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cardRequestUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardrequest`
--

LOCK TABLES `cardrequest` WRITE;
/*!40000 ALTER TABLE `cardrequest` DISABLE KEYS */;
INSERT INTO `cardrequest` VALUES (12,1,1,'Diamond',5000,'2018-01-21'),(13,8,4,'Gold',175000,'2018-01-21'),(14,8,4,'Gold',200000,'2018-01-21'),(15,1,5,'Diamond',99999,'2018-01-26');
/*!40000 ALTER TABLE `cardrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creditcard`
--

DROP TABLE IF EXISTS `creditcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creditcard` (
  `cardID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `cardNumber` varchar(50) NOT NULL,
  `cardType` varchar(20) NOT NULL,
  `creditLimit` double NOT NULL,
  `expiryDate` date DEFAULT NULL,
  `creditAmount` double DEFAULT NULL,
  `debitAmount` double DEFAULT NULL,
  PRIMARY KEY (`cardID`),
  UNIQUE KEY `cardNumber_UNIQUE` (`cardNumber`),
  KEY `ccUserID_idx` (`userID`),
  CONSTRAINT `ccUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creditcard`
--

LOCK TABLES `creditcard` WRITE;
/*!40000 ALTER TABLE `creditcard` DISABLE KEYS */;
INSERT INTO `creditcard` VALUES (1,1,'2104 1235 2545 2355','Gold',50000,'2018-01-03',0,500),(2,8,'3214 2345 5324 1566','Diamond',75000,'2018-01-03',0,8264),(3,8,'2344 2345 6457 8768','Diamond',75000,'2018-06-21',0,0),(4,1,'2423 5464 2536 3456','Platinum',50000,'2018-01-31',2000,0),(5,1,'1243 2356 3657 5476','Diamond',100000,'2018-05-24',0,0),(6,8,'2354 1358 1489 1085','Gold',120000,'2018-06-27',0,0),(7,8,'3566 5415 5679 1578','Platinum',75000,'2018-12-31',0,0),(8,1,'1234 3244 3535 4355','Platinum',150000,'2018-06-12',0,0),(9,1,'1234 1234 1234 1234','Gold',250000,'2018-01-01',0,0);
/*!40000 ALTER TABLE `creditcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `senderID` int(11) NOT NULL,
  `recipientID` int(11) NOT NULL,
  `messageSubject` varchar(225) NOT NULL,
  `messageBody` varchar(225) NOT NULL,
  `messageDate` varchar(100) NOT NULL,
  PRIMARY KEY (`messageID`),
  KEY `senderID_idx` (`senderID`),
  KEY `recipientID_idx` (`recipientID`),
  CONSTRAINT `recipientID` FOREIGN KEY (`recipientID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `senderID` FOREIGN KEY (`senderID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,2,1,'Welcome to Startrust!','Hey there, We warmly welcome you to Startrust Bank! We hope you enjoy the stay. Meanwhile please make sure to change your password to ensure maximum security. Also, if you have any concern leave a message.','2018/01/08 12:24:48'),(2,1,2,'Thank you!','Thank you so much! I plan on requesting a credit card through the system and is unaware of the procedure. Could you help me through this process?\r\nThanks in advance.','2018/01/08 18:20:08'),(3,1,3,'Credit Card Request','Hey there! I have requested a credit card from the portal. Could you please look in to the request?\r\nThanks in advance.','2018/01/08 18:32:57'),(4,1,2,'Feedback Form','I was informed of the module learning outcomes. I understood the concepts explained.','2018/01/09 08:58:16'),(5,1,3,'Thank you','Thank you for the Credit Card approval. Much appreciated!','2018/01/12 10:25:46'),(14,1,3,'Hey there!','Hey there I recently joined Startrust bank!','2018/01/21 11:48:13'),(17,3,1,'Hey there!','Welcome to Startrust!','2018/01/25 11:11:27'),(18,1,8,'Hi','Hi','2018/01/26 04:16:54'),(19,8,1,'Hi too','Hi too baeb','2018/01/26 04:28:33'),(20,1,9,'Hello','Helloooo\r\n','2018/01/26 04:35:04'),(21,9,1,'Hi','Hi','2018/01/26 13:09:22');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentprofile`
--

DROP TABLE IF EXISTS `paymentprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentprofile` (
  `profileID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `profileName` varchar(50) NOT NULL,
  `profileType` varchar(50) NOT NULL,
  `accountNumber` varchar(100) NOT NULL,
  PRIMARY KEY (`profileID`),
  KEY `paymentProfileUserID_idx` (`userID`),
  CONSTRAINT `paymentProfileUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentprofile`
--

LOCK TABLES `paymentprofile` WRITE;
/*!40000 ALTER TABLE `paymentprofile` DISABLE KEYS */;
INSERT INTO `paymentprofile` VALUES (1,1,'Home','Electricity','1235'),(2,1,'Medical','Insurance','25658'),(3,8,'Mom Phone','Telephone','12334324'),(4,1,'Home Water','Water','35424'),(5,1,'Dish TV','Cable TV','3452435'),(6,1,'SLT Home','Internet','435532'),(7,8,'SLT Fibre','Internet','3245235N'),(8,1,'Home SLT','Telephone','213445');
/*!40000 ALTER TABLE `paymentprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resetpassword`
--

DROP TABLE IF EXISTS `resetpassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resetpassword` (
  `resetID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `tempPassword` varchar(200) NOT NULL,
  PRIMARY KEY (`resetID`),
  UNIQUE KEY `userID_UNIQUE` (`userID`),
  KEY `resetUserID_idx` (`userID`),
  CONSTRAINT `resetUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resetpassword`
--

LOCK TABLES `resetpassword` WRITE;
/*!40000 ALTER TABLE `resetpassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `resetpassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standingorder`
--

DROP TABLE IF EXISTS `standingorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standingorder` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `sourceNum` varchar(50) NOT NULL,
  `destNum` varchar(50) NOT NULL,
  `destType` varchar(50) NOT NULL,
  `orderAmount` double NOT NULL,
  `startDate` date NOT NULL,
  `orderDuration` int(11) NOT NULL,
  `durationType` varchar(50) NOT NULL,
  PRIMARY KEY (`orderID`),
  KEY `soUserID_idx` (`userID`),
  CONSTRAINT `soUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standingorder`
--

LOCK TABLES `standingorder` WRITE;
/*!40000 ALTER TABLE `standingorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `standingorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactionhistory`
--

DROP TABLE IF EXISTS `transactionhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactionhistory` (
  `transactionID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `sourceNum` varchar(50) NOT NULL,
  `destinationNum` varchar(50) NOT NULL,
  `transferAmt` double NOT NULL,
  `transferType` varchar(50) DEFAULT NULL,
  `transferDate` date NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `transactionUserID_idx` (`userID`),
  CONSTRAINT `transactionUserID` FOREIGN KEY (`userID`) REFERENCES `useraccount` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactionhistory`
--

LOCK TABLES `transactionhistory` WRITE;
/*!40000 ALTER TABLE `transactionhistory` DISABLE KEYS */;
INSERT INTO `transactionhistory` VALUES (1,1,'2354678','2354454',277,'Own','2018-01-20'),(2,1,'2354678','2354454',1000,'Own','2018-01-20'),(3,8,'786124','12334324',500,'Telephone','2018-01-21'),(4,1,'2354678','672131476',5000,'Other','2018-01-21'),(5,1,'2354454','3534536645',5000,'Own','2018-01-22'),(6,8,'786124','3245235N',4700,'Internet','2018-01-24'),(7,1,'2354678','0777776772',7000,'Other','2018-01-25'),(8,1,'2354678','2354454',3000,'Own','2018-01-25'),(9,1,'2354678','786124',7000,'Startrust','2018-01-25'),(10,1,'2354678','213445',10000,'Telephone','2018-01-26'),(11,1,'2354454','2354678',200000,'Own','2018-01-26'),(12,1,'2354454','786124',100000,'Startrust','2018-01-26'),(13,9,'1246546','3534536645',50000,'Startrust','2018-01-26'),(14,1,'2354678','2104 1235 2545 2355',38000,'Credit Card','2018-01-26');
/*!40000 ALTER TABLE `transactionhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useraccount`
--

DROP TABLE IF EXISTS `useraccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useraccount` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL,
  `userEmail` varchar(100) NOT NULL,
  `userType` varchar(20) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) NOT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `identityNumber` varchar(50) NOT NULL,
  `passportNumber` varchar(50) DEFAULT NULL,
  `userGender` varchar(1) NOT NULL,
  `userAddress` varchar(100) NOT NULL,
  `userCity` varchar(50) NOT NULL,
  `zipCode` varchar(50) DEFAULT NULL,
  `mobileNumber` varchar(20) DEFAULT NULL,
  `homeNumber` varchar(20) DEFAULT NULL,
  `profileImg` varchar(255) DEFAULT NULL,
  `userPassword` varchar(255) NOT NULL,
  `lastLogin` date DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  UNIQUE KEY `userEmail_UNIQUE` (`userEmail`),
  UNIQUE KEY `identityNumber_UNIQUE` (`identityNumber`),
  UNIQUE KEY `passportNumber_UNIQUE` (`passportNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useraccount`
--

LOCK TABLES `useraccount` WRITE;
/*!40000 ALTER TABLE `useraccount` DISABLE KEYS */;
INSERT INTO `useraccount` VALUES (1,'shaanxd','hassan.shaan31@gmail.com','customer','Shahid','Mohamed','Hassan','1996-11-15','963200072V','','M','434/B, Enderamulla','Wattala','11300','0774406047','0112942408',NULL,'896451711e4594f9908ce6eb5990725d','2018-01-26'),(2,'nateD','nathanDrake@startrust.com','admin','Nathan','Morgan','Drake','1982-01-11','820110001V',NULL,'M','45/C, Samagi Mawatha','Dehiwala','12100','0776632003','0112520127','','27261139b6e87cbfe8922c9d45f16422','2018-01-26'),(3,'nicoB','nicoBellic@startrust.com','manager','Nikolai',NULL,'Bellic','1978-02-06','783702491V',NULL,'M','34/A, Jude Rd','Nugegoda','13400','0777776772','0112925123',NULL,'4dd780e43e45859aa09bce8e65246069','2018-01-26'),(8,'schwoulin','schwoulin@gmail.com','customer','Fathimath','','Hanau','1992-02-02','923300072V','50SD234','F','15/4, Kawdana Road','Dehiwala','13100','0712254477','0111234567',NULL,'6a48030d70aed4807a634e4b67c4b3de','2018-01-26'),(9,'rishanxd','mastergamerme@gmail.com','customer','Rishan','','Faliq','2069-06-09','34534534v','1244353','M','43, Nugegoda','Nugegoda','124324','077123456','1234566',NULL,'f4ad231214cb99a985dff0f056a36242','2018-01-26');
/*!40000 ALTER TABLE `useraccount` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-26 13:25:39
