/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Shaan
 */
public class passwordEncryption {
    public static String encrypt(String password) throws NoSuchAlgorithmException{
        String encryptPassword=password;
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(encryptPassword.getBytes(),0,encryptPassword.length());
        return(new BigInteger(1,m.digest()).toString(16));
     }
}
