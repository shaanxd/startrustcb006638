/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.accountDetails;

/**
 *
 * @author Shaan
 */
public class myAccountsBean implements Serializable{
    List <accountDetails> accountList = new ArrayList<>();
    
    public void addAccount(String accountID, String name, String accountNumber, double accountBalance, String branchName, double interestGained, String accountType) {
        accountList.add(new accountDetails(accountID, name, accountNumber, accountBalance, branchName, interestGained, accountType));
    }

    public List<accountDetails> getAccountList() {
        return accountList;
    }
    
}
