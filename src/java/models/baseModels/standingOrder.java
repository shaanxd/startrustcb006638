/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class standingOrder implements Serializable{
    String orderID, sourceNum, destNum, destType, startDate, durationType;
    int orderDuration;
    double orderAmount;

    public standingOrder(String orderID, String sourceNum, String destNum, String destType, String startDate, String durationType, int orderDuration, double orderAmount) {
        this.orderID = orderID;
        this.sourceNum = sourceNum;
        this.destNum = destNum;
        this.destType = destType;
        this.startDate = startDate;
        this.durationType = durationType;
        this.orderDuration = orderDuration;
        this.orderAmount = orderAmount;
    }

    public String getOrderID() {
        return orderID;
    }

    public String getSourceNum() {
        return sourceNum;
    }

    public String getDestNum() {
        return destNum;
    }

    public String getDestType() {
        return destType;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getDurationType() {
        return durationType;
    }

    public int getOrderDuration() {
        return orderDuration;
    }

    public double getOrderAmount() {
        return orderAmount;
    }
    
}
