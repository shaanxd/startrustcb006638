/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class accountDetails implements Serializable{
        String accountID;
        String name;
        String accountNumber;
        double accountBalance;
        String branchName;
        double interestGained;
        String accountType;

        public accountDetails(String accountID, String name, String accountNumber,String accountBalance) {
            this.accountID = accountID;
            this.name = name;
            this.accountNumber = accountNumber;
            this.accountBalance = Double.parseDouble(accountBalance);
        }

        public accountDetails(String accountID, String name, String accountNumber, double accountBalance, String branchName, double interestGained, String accountType) {
            this.accountID = accountID;
            this.name = name;
            this.accountNumber = accountNumber;
            this.accountBalance = accountBalance;
            this.branchName = branchName;
            this.interestGained = interestGained;
            this.accountType = accountType;
        }

        public accountDetails(String accountID, String name, String accountNumber, double accountBalance) {
            this.accountID = accountID;
            this.name = name;
            this.accountNumber = accountNumber;
            this.accountBalance = accountBalance;
        }
        
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }      

        public double getAccountBalance() {
            return accountBalance;
        }

        public void setAccountBalance(double accountBalance) {
            this.accountBalance = accountBalance;
        }

        public String getAccountID() {
            return accountID;
        }

        public void setAccountID(String accountID) {
            this.accountID = accountID;
        }

        public String getBranchName() {
            return branchName;
        }

        public double getInterestGained() {
            return interestGained;
        }

        public String getAccountType() {
            return accountType;
        }
        
}
