/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class paymentProfile implements Serializable{
    String profileID,profileName,profileType,accountNum;

    public paymentProfile(String profileID, String profileName, String profileType, String accountNum) {
        this.profileID = profileID;
        this.profileName = profileName;
        this.profileType = profileType;
        this.accountNum = accountNum;
    }

    public String getProfileID() {
        return profileID;
    }
    
    public String getProfileName() {
        return profileName;
    }

    public String getProfileType() {
        return profileType;
    }

    public String getAccountNum() {
        return accountNum;
    }
    
}
