/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class userMessage implements Serializable{
    
        String otherUser;
        String subject;
        String body;
        String date;

        public userMessage(String otherUser, String subject, String body, String date) {
            this.otherUser = otherUser;
            this.subject = subject;
            this.body = body;
            this.date = date;
        }

        public String getOtherUser() {
            return otherUser;
        }

        public String getSubject() {
            return subject;
        }

        public String getBody() {
            return body;
        }

        public String getDate() {
            return date;
        }
        
}
