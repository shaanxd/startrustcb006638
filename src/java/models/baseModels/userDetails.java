/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class userDetails implements Serializable{
    String userID;
    String userName;
    String firstName;
    String lastName;
    String userEmail;
    String lastLogin;

    public userDetails(String userID, String userName) {
        this.userID = userID;
        this.userName = userName;
    }

    public userDetails(String userID, String firstName, String lastName, String userEmail, String lastLogin) {
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userEmail = userEmail;
        this.lastLogin = lastLogin;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getLastLogin() {
        return lastLogin;
    }
    
    
}
