/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class transactionDetails implements Serializable{
    String sourceNum, destinationNum, transferType, transferDate;
    double transferAmt;

    public transactionDetails(String sourceNum, String destinationNum, String transferType, String transferDate, double transferAmt) {
        this.sourceNum = sourceNum;
        this.destinationNum = destinationNum;
        this.transferType = transferType;
        this.transferDate = transferDate;
        this.transferAmt = transferAmt;
    }

    public String getSourceNum() {
        return sourceNum;
    }

    public String getDestinationNum() {
        return destinationNum;
    }

    public String getTransferType() {
        return transferType;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public double getTransferAmt() {
        return transferAmt;
    }

    
    
}
