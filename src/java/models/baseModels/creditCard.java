/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class creditCard implements Serializable{
    String cardType, cardNumber;
    String expiryDate;
    Double creditAmount, debitAmount, creditLimit;
    String percentage;

    public creditCard(String cardType, String cardNumber) {
        this.cardType = cardType;
        this.cardNumber = cardNumber;
    }

    public creditCard(String cardType, String cardNumber, String expiryDate, Double creditAmount, Double debitAmount, Double creditLimit) {
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
        this.creditAmount = creditAmount;
        this.debitAmount = debitAmount;
        this.creditLimit = creditLimit;
        this.percentage = ((creditAmount/creditLimit)*100)+"%";
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public Double getCreditAmount() {
        return creditAmount;
    }

    public Double getDebitAmount() {
        return debitAmount;
    }

    public Double getCreditLimit() {
        return creditLimit;
    }

    public String getPercentage() {
        return percentage;
    }
    
}
