/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class cardRequest implements Serializable{
    String requestID, username, cardType, requestDate;
    double creditLimit;

    public cardRequest(String requestID, String username, String cardType, double creditLimit, String requestDate) {
        this.requestID = requestID;
        this.username = username;
        this.cardType = cardType;
        this.requestDate = requestDate;
        this.creditLimit = creditLimit;
    }

    public cardRequest(String username, String cardType, String requestDate) {
        this.username = username;
        this.cardType = cardType;
        this.requestDate = requestDate;
    }
    
    public String getRequestID() {
        return requestID;
    }

    public String getUsername() {
        return username;
    }

    public String getCardType() {
        return cardType;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public double getCreditLimit() {
        return creditLimit;
    }
    
}
