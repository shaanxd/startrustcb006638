/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class beneficiaryClass implements Serializable{
    String beneficiaryID;
    String accountNumber;
    String accountName;
    String accountType;
    String benBank;
    String benBranch;

    public beneficiaryClass(String beneficiaryID, String accountNumber, String accountName,String accountType) {
        this.beneficiaryID = beneficiaryID;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.accountType = accountType;
    }

    public beneficiaryClass(String beneficiaryID, String accountNumber, String accountName) {
        this.beneficiaryID = beneficiaryID;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
    }

    public beneficiaryClass(String beneficiaryID, String accountNumber, String accountName, String accountType, String benBank, String benBranch) {
        this.beneficiaryID = beneficiaryID;
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.accountType = accountType;
        this.benBank = benBank;
        this.benBranch = benBranch;
    }
    
    public String getBeneficiaryID() {
        return beneficiaryID;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getBenBank() {
        return benBank;
    }

    public String getBenBranch() {
        return benBranch;
    }
    
}
