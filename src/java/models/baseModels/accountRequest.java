/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.baseModels;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class accountRequest implements Serializable{
    String requestID,name,date,accType,branchName;

    public accountRequest(String requestID, String name, String date, String accType, String branchName) {
        this.requestID = requestID;
        this.name = name;
        this.date = date;
        this.accType = accType;
        this.branchName = branchName;
    }

    public String getRequestID() {
        return requestID;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getAccType() {
        return accType;
    }

    public String getBranchName() {
        return branchName;
    }
    
    
}
