/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Shaan
 */
public class registerAccountBean implements Serializable {
    String username,firstname,middlename,lastname,email;
    String idNum, passportNum, gender, address, city, zipCode, mobileNum, homeNum;
    String accountNum, accountType, accountBranch,accountName;
    Double initialDeposit;
    Date dateOfBirth;
    String userID;

    public registerAccountBean(String username, String firstname, String middlename, String lastname, String email, String idNum, String passportNum, String gender, String address, String city, String zipCode, String mobileNum, String homeNum, String accountNum, String accountType, String accountBranch, Double initialDeposit, Date dateOfBirth,String accountName) {
        this.username = username;
        this.firstname = firstname;
        this.middlename = middlename;
        this.lastname = lastname;
        this.email = email;
        this.idNum = idNum;
        this.passportNum = passportNum;
        this.gender = gender;
        this.address = address;
        this.city = city;
        this.zipCode = zipCode;
        this.mobileNum = mobileNum;
        this.homeNum = homeNum;
        this.accountNum = accountNum;
        this.accountType = accountType;
        this.accountBranch = accountBranch;
        this.initialDeposit = initialDeposit;
        this.dateOfBirth = dateOfBirth;
        this.accountName = accountName;
    }

    public String getAccountName() {
        return accountName;
    }
    
    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getIdNum() {
        return idNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public String getHomeNum() {
        return homeNum;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getAccountBranch() {
        return accountBranch;
    }

    public Double getInitialDeposit() {
        return initialDeposit;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    
    
    
}
