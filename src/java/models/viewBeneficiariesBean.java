/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.beneficiaryClass;

/**
 *
 * @author Shaan
 */
public class viewBeneficiariesBean implements Serializable{
    
    List <beneficiaryClass> benList = new ArrayList<>();
    
    public void addBen(String beneficiaryID, String accountNumber, String accountName, String accountType, String benBank, String benBranch) {
        benList.add(new beneficiaryClass(beneficiaryID, accountNumber, accountName, accountType, benBank, benBranch));
    }

    public List<beneficiaryClass> getBenList() {
        return benList;
    }
    
}
