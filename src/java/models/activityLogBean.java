/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.userDetails;

/**
 *
 * @author Shaan
 */
public class activityLogBean implements Serializable {
    List<userDetails> userList = new ArrayList<>();
    List<activityLogBean> activityList = new ArrayList<>();
    
    String activityType,  activityDescription, cardType;
    String dateTime, userID;
    
    public void addActivity(String activityType, String activityDescription, String dateTime){
        activityList.add(new activityLogBean(activityType, activityDescription, dateTime));
    }
    
    public activityLogBean(String activityType){
        this.activityType = activityType;
    }
    
    public activityLogBean(String activityType, String activityDescription, String dateTime){
        this.activityType = activityType;
        this.activityDescription = activityDescription;
        this.dateTime = dateTime;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public void setActivityType(String activityType){
        this.activityType = activityType;
    }
    
    public void setActivityDescription(){
        if(this.activityType.equals("Account Request")){
            activityDescription = "Requested a bank account from the portal";
        }
        else if(this.activityType.equals("Card Request")){
            activityDescription = "Requested a "+cardType+" card from the portal";
        }
        else if(this.activityType.equals("Payment Profile")){
            activityDescription = "Added a payment profile to the portal";
        }
        else if(this.activityType.equals("Login")){
            activityDescription = "Logged in successfully";
        }
        else if(this.activityType.equals("Updated Profile")){
            activityDescription = "Updated profile details";
        }
        else if(this.activityType.equals("Logout")){
            activityDescription = "Logged out successfully";
        }
    }
    
    public String getActivityType(){
        return activityType;
    }
    
    public String getActivityDescription(){
        return activityDescription;
    }
    
    public void addUser(String userID, String userName){
        userList.add(new userDetails(userID, userName));
    }

    public activityLogBean() {
    }

    public List<userDetails> getUserList() {
        return userList;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<activityLogBean> getActivityList() {
        return activityList;
    }

    public String getDateTime() {
        return dateTime;
    }
    
}
