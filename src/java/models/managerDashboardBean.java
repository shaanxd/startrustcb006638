/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.cardRequest;
import models.baseModels.userMessage;

/**
 *
 * @author Shaan
 */
public class managerDashboardBean implements Serializable{
    List <cardRequest> requestList = new ArrayList<>();
    List <userMessage> inboxList = new ArrayList<>();
    
    public void addInboxListItem(String recipient, String subject, String body, String date){
        inboxList.add(new userMessage(recipient,subject,body,date));
    }

    public List<userMessage> getInboxList() {
        return inboxList;
    }
    
    public void addRequest(String username, String cardType, String requestDate) {
        requestList.add(new cardRequest(username, cardType, requestDate));
    }

    public List<cardRequest> getRequestList() {
        return requestList;
    }
    
}
