/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class addPaymentProfileBean implements Serializable{
    String profileName, accountNumber, profileType;

    public addPaymentProfileBean(String profileName, String accountNumber, String profileType) {
        this.profileName = profileName;
        this.accountNumber = accountNumber;
        this.profileType = profileType;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }
    
    
}
