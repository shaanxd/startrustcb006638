/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class resetPasswordBean implements Serializable{
    String resetID;
    String userID;
    String password;

    public String getResetID() {
        return resetID;
    }

    public void setResetID(String resetID) {
        this.resetID = resetID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword() {
        this.password = Long.toHexString(Double.doubleToLongBits(Math.random()));
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
