/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Shaan
 */
public class addBeneficiaryBean implements Serializable{
    String benType,benName,benNum,benBank,benBranch;

    public addBeneficiaryBean(String benType, String benName, String benNum) {
        this.benType = benType;
        this.benName = benName;
        this.benNum = benNum;
    }

    public addBeneficiaryBean(String benType, String benName, String benNum, String benBank, String benBranch) {
        this.benType = benType;
        this.benName = benName;
        this.benNum = benNum;
        this.benBank = benBank;
        this.benBranch = benBranch;
    }

    public String getBenType() {
        return benType;
    }

    public String getBenName() {
        return benName;
    }

    public String getBenNum() {
        return benNum;
    }

    public String getBenBank() {
        return benBank;
    }

    public String getBenBranch() {
        return benBranch;
    }
    
}
