/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.baseModels.*;

/**
 *
 * @author Shaan
 */
public class addStandingOrderBean implements Serializable{
    
    List <accountDetails> accountList = new ArrayList<>();
    List <paymentProfile> profileList = new ArrayList<>();
    List <creditCard> cardList = new ArrayList<>();
    List <beneficiaryClass> benList = new ArrayList<>();
    
    String sourceNum, destNum, destType,durType,duration;
    double orderAmt;
    Date startDate;

    public addStandingOrderBean(String sourceNum, String durType, double orderAmt, Date startDate, String duration) {
        this.sourceNum = sourceNum;
        this.durType = durType;
        this.orderAmt = orderAmt;
        this.startDate = startDate;
        this.duration = duration;
    }

    public addStandingOrderBean() {
        
    }

    public String getSourceNum() {
        return sourceNum;
    }

    public String getDestNum() {
        return destNum;
    }

    public String getDestType() {
        return destType;
    }

    public String getDurType() {
        return durType;
    }

    public String getDuration() {
        return duration;
    }

    public double getOrderAmt() {
        return orderAmt;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setDestNum(String destNum) {
        this.destNum = destNum;
    }

    public void setDestType(String destType) {
        this.destType = destType;
    }
    
    public List<accountDetails> getAccountList() {
        return accountList;
    }

    public List<paymentProfile> getProfileList() {
        return profileList;
    }

    public List<creditCard> getCardList() {
        return cardList;
    }    

    public List<beneficiaryClass> getBenList() {
        return benList;
    }
    
    public void addProfileList(String id, String name, String type, String accNum){
        profileList.add(new paymentProfile(id, name, type, accNum));
    }
    
    public void addCardList(String type, String number){
        cardList.add(new creditCard( type, number));
    }
    
    public void addAccountList(String accountID, String name, String accountNumber,String accountBalance){
        accountList.add(new accountDetails(accountID,name,accountNumber,accountBalance));
    }
    
    public void addBenList(String benID, String benNumber, String benName, String benType){
        benList.add(new beneficiaryClass(benID, benNumber, benName, benType));
    }
}
