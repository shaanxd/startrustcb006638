/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Shaan
 */
public class interestRateBean {
    double InterestRate;
    String accountType;

    public interestRateBean(double InterestRate, String accountType) {
        this.InterestRate = InterestRate;
        this.accountType = accountType;
    }

    public double getInterestRate() {
        return InterestRate;
    }

    public String getAccountType() {
        return accountType;
    }
    
}
