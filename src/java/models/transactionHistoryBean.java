/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.baseModels.transactionDetails;

/**
 *
 * @author Shaan
 */
public class transactionHistoryBean implements Serializable{
    Date fromDate;
    Date toDate;

    List<transactionDetails> transactionList = new ArrayList<>();
    
    public transactionHistoryBean(Date fromDate, Date toDate) {
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public void addTransaction(String sourceNum, String destinationNum, String transferType, String transferDate, double transferAmt) {
        transactionList.add(new transactionDetails(sourceNum, destinationNum, transferType, transferDate, transferAmt));
    }
    
    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public List<transactionDetails> getTransactionList() {
        return transactionList;
    }
    
    
}
