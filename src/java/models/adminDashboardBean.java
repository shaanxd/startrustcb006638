/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.userMessage;

/**
 *
 * @author Shaan
 */
public class adminDashboardBean implements Serializable{
    String currentRate;
    String savingsRate;
    String fdRate;
    
    List <userMessage> inboxList = new ArrayList<>();

    public String getCurrentRate() {
        return currentRate;
    }

    public String getSavingsRate() {
        return savingsRate;
    }

    public String getFdRate() {
        return fdRate;
    }

    public void setCurrentRate(String currentRate) {
        this.currentRate = currentRate;
    }

    public void setSavingsRate(String savingsRate) {
        this.savingsRate = savingsRate;
    }

    public void setFdRate(String fdRate) {
        this.fdRate = fdRate;
    }
    
    public void addInboxListItem(String recipient, String subject, String body, String date){
        inboxList.add(new userMessage(recipient,subject,body,date));
    }

    public List<userMessage> getInboxList() {
        return inboxList;
    }
    
}
