/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.creditCard;

/**
 *
 * @author Shaan
 */
public class viewCardsBean implements Serializable{
    List <creditCard> cardList = new ArrayList<>();
    String displayName;
    
    public void addCard(String cardType, String cardNumber, String expiryDate, double creditAmount, double debitAmount, double creditLimit) {
        cardList.add(new creditCard(cardType, cardNumber, expiryDate, creditAmount, debitAmount, creditLimit));
    }

    public List<creditCard> getCardList() {
        return cardList;
    }
    
    public void setName(String firstName, String lastName){
        displayName = firstName.charAt(0)+". "+lastName;
    }

    public String getDisplayName() {
        return displayName;
    }
    
}
