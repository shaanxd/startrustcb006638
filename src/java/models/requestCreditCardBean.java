/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.accountDetails;

/**
 *
 * @author Shaan
 */
public class requestCreditCardBean implements Serializable {
    
    List <accountDetails> accountList = new ArrayList<>();
    String accountID, cardType, creditLimit;

    public requestCreditCardBean(String accountID, String cardType, String creditLimit) {
        this.accountID = accountID;
        this.cardType = cardType;
        this.creditLimit = creditLimit;
    }

    public requestCreditCardBean() {
    }
    
    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }
    
    public List<accountDetails> getAccountList() {
        return accountList;
    }
    
    public void addAccountList(String accountID, String name, String accountNumber,String accountBalance){
        accountList.add(new accountDetails(accountID,name,accountNumber,accountBalance));
    }
    
}
