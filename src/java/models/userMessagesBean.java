/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.userMessage;

/**
 *
 * @author Shaan
 */
public class userMessagesBean implements Serializable{
    String recipientID; String messageSubject; String messageBody;
    List <String> userList = new ArrayList<>();
    List <userMessage> inboxList = new ArrayList<>();
    List <userMessage> outboxList = new ArrayList<>();

    public String getRecipientID() {
        return recipientID;
    }

    public void addUser(String user){
        userList.add(user);
    }    
    public void addInboxListItem(String recipient, String subject, String body, String date){
        inboxList.add(new userMessage(recipient,subject,body,date));
    }
    
    public void addOutBoxListItem(String sender, String subject, String body, String date){
        outboxList.add(new userMessage(sender,subject,body,date));
    }
    
    //getters and setters
    public void setRecipientID(String recipientID) {
        this.recipientID = recipientID;
    }
    public String getMessageSubject() {
        return messageSubject;
    }
    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }
    public String getMessageBody() {
        return messageBody;
    }
    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
    public List<String> getUserList() {
        return userList;
    }
    public List<userMessage> getInboxList() {
        return inboxList;
    }
    public List<userMessage> getOutboxList() {
        return outboxList;
    }
    
}
