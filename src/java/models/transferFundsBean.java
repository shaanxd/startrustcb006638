/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import models.baseModels.beneficiaryClass;
import models.baseModels.accountDetails;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shaan
 */
public class transferFundsBean implements Serializable{
    List <beneficiaryClass> stList = new ArrayList<>();
    List <beneficiaryClass> extList = new ArrayList<>();
    List <accountDetails> accountList = new ArrayList<>();
    
    String sourceAcctNum, destAcctNum;
    double sourceInitialAmount, destInitialAmount, deductAmount;

    public String getSourceAcctNum() {
        return sourceAcctNum;
    }

    public void setSourceAcctNum(String sourceAcctNum) {
        this.sourceAcctNum = sourceAcctNum;
    }

    public String getDestAcctNum() {
        return destAcctNum;
    }

    public void setDestAcctNum(String destAcctNum) {
        this.destAcctNum = destAcctNum;
    }
    
    public void addSt(String beneficiaryID, String accountNumber,String accountName){
        stList.add(new beneficiaryClass(beneficiaryID, accountNumber, accountName));
    }
    public void addExt(String beneficiaryID, String accountNumber,String accountName,String accountType){
        extList.add(new beneficiaryClass(beneficiaryID, accountNumber, accountName, accountType));
    }

    public void addAccountList(String accountID, String name, String accountNumber,String accountBalance){
        accountList.add(new accountDetails(accountID,name,accountNumber,accountBalance));
    }
    
    public List<beneficiaryClass> getStList() {
        return stList;
    }

    public List<beneficiaryClass> getExtList() {
        return extList;
    }

    public List<accountDetails> getAccountList() {
        return accountList;
    }

    public double getSourceInitialAmount() {
        return sourceInitialAmount;
    }

    public void setSourceInitialAmount(double sourceInitialAmount) {
        this.sourceInitialAmount = sourceInitialAmount;
    }

    public double getDestInitialAmount() {
        return destInitialAmount;
    }

    public void setDestInitialAmount(double destInitialAmount) {
        this.destInitialAmount = destInitialAmount;
    }

    public double getDeductAmount() {
        return deductAmount;
    }

    public void setDeductAmount(double deductAmount) {
        this.deductAmount = deductAmount;
    }

    public double reduceAmount(){
        return(sourceInitialAmount - deductAmount);
    }
    public double addAmount(){
        return(destInitialAmount+deductAmount);
    }
    
}
