/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.List;
import models.baseModels.accountDetails;
import models.baseModels.transactionDetails;
import models.baseModels.userMessage;

/**
 *
 * @author Shaan
 */
public class userDashboardBean {
    List <accountDetails> accountList = new ArrayList<>();
    List <userMessage> inboxList = new ArrayList<>();
    List<transactionDetails> transactionList = new ArrayList<>();
    
    public void addAccount(String accountID, String name, String accountNumber, double accountBalance) {
        accountList.add(new accountDetails(accountID, name, accountNumber, accountBalance));
    }

    public List<accountDetails> getAccountList() {
        return accountList;
    }
    
    public void addInboxListItem(String recipient, String subject, String body, String date){
        inboxList.add(new userMessage(recipient,subject,body,date));
    }

    public List<userMessage> getInboxList() {
        return inboxList;
    }
    
    public void addTransaction(String sourceNum, String destinationNum, String transferType, String transferDate, double transferAmt) {
        transactionList.add(new transactionDetails(sourceNum, destinationNum, transferType, transferDate, transferAmt));
    }
    
    public List<transactionDetails> getTransactionList() {
        return transactionList;
    }
    
}
