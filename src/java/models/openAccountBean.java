/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.accountDetails;

/**
 *
 * @author Shaan
 */
public class openAccountBean implements Serializable{
    
    List <accountDetails> accountList = new ArrayList<>();

    String accountType, sourceAccount, branchName,accName;
    double initialDeposit;

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }
    
    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public double getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(double initialDeposit) {
        this.initialDeposit = initialDeposit;
    }
    
    public List<accountDetails> getAccountList() {
        return accountList;
    }
    
    public void addAccountList(String accountID, String name, String accountNumber,String accountBalance){
        accountList.add(new accountDetails(accountID,name,accountNumber,accountBalance));
    }
}
