/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import models.baseModels.accountRequest;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shaan
 */
public class approveAccountsBean implements Serializable{
    List <accountRequest> requestList = new ArrayList <>();
    String requestID,accountNum,feeAccount,userID,accountType,accountName, branchName;
    double initDeposit, sourceAccAmount;

    public void addInfo(String feeAccount, String userID, String accountType, String accountName, String branchName, double initDeposit) {
        this.feeAccount = feeAccount;
        this.userID = userID;
        this.accountType = accountType;
        this.accountName = accountName;
        this.branchName = branchName;
        this.initDeposit = initDeposit;
    }

    public double calcAmount(){
        return(sourceAccAmount-initDeposit);
    }
    
    public boolean compareVals(){
        if(initDeposit>sourceAccAmount){
            return true;
        }
        else{
            return false;
        }
    }
    
    public double getSourceAccAmount() {
        return sourceAccAmount;
    }

    public void setSourceAccAmount(double sourceAccAmount) {
        this.sourceAccAmount = sourceAccAmount;
    }
    
    public String getFeeAccount() {
        return feeAccount;
    }

    public String getUserID() {
        return userID;
    }

    public String getAccountType() {
        return accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getBranchName() {
        return branchName;
    }

    public double getInitDeposit() {
        return initDeposit;
    }
    
    public String getAccountNum() {
        return accountNum;
    }
    
    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }
    
    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
    
    public void addRequest(String requestID,  String name, String date, String accType, String branchName){
        requestList.add(new accountRequest(requestID,name,date,accType,branchName));
    }

    public List<accountRequest> getRequestList() {
        return requestList;
    }
    
}
