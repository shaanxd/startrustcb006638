/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.cardRequest;

/**
 *
 * @author Shaan
 */
public class approveCardsBean implements Serializable{
    List <cardRequest> requestList = new ArrayList<>();
    String requestID, cardNum, accountID, cardType, userID, expiryDate;
    double creditLimit;
    double balanceAmount;
    
    public void addRequest(String requestID, String username, String cardType, double creditLimit, String requestDate){
        requestList.add(new cardRequest(requestID, username, cardType, creditLimit, requestDate));
    }

    public List<cardRequest> getRequestList() {
        return requestList;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }
    
    public void addInfo(String accountID, String cardType, double creditLimit,String userID){
        this.accountID = accountID;
        this.cardType = cardType;
        this.creditLimit = creditLimit;
        this.userID = userID;
    }

    public String getAccountID() {
        return accountID;
    }

    public String getCardType() {
        return cardType;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
    
    public boolean checkBalance(){
        if(balanceAmount<200){
            return false;
        }
        else{
            return true;
        }
    }
    
    public double calcBalance(){
        return(balanceAmount-200);
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getUserID() {
        return userID;
    }
   
}
