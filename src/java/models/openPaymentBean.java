/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import models.baseModels.creditCard;
import models.baseModels.accountDetails;
import models.baseModels.paymentProfile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Shaan
 */
public class openPaymentBean implements Serializable{
    List <paymentProfile> profileList = new ArrayList<>();
    List <accountDetails> accountList = new ArrayList<>();
    List <creditCard> cardList = new ArrayList<>();
    
    String sourceNum, destinationNum;
    double amount, sourceInit;
    double creditAmt, debitAmt;
    String paymentType;

    public openPaymentBean() {
    }

    public openPaymentBean(String sourceNum, String destinationNum, double amount) {
        this.sourceNum = sourceNum;
        this.destinationNum = destinationNum;
        this.amount = amount;
    }
    
    public void addAccountList(String accountID, String name, String accountNumber,String accountBalance){
        accountList.add(new accountDetails(accountID,name,accountNumber,accountBalance));
    }
    
    public void addProfileList(String id, String name, String type, String accNum){
        profileList.add(new paymentProfile(id, name, type, accNum));
    }
    
    public void addCardList(String type, String number){
        cardList.add(new creditCard( type, number));
    }
    
    public List<accountDetails> getAccountList() {
        return accountList;
    }

    public List<paymentProfile> getProfileList() {
        return profileList;
    }

    public List<creditCard> getCardList() {
        return cardList;
    }

    public String getSourceNum() {
        return sourceNum;
    }

    public String getDestinationNum() {
        return destinationNum;
    }

    public double getAmount() {
        return amount;
    }

    public double getSourceInit() {
        return sourceInit;
    }

    public void setSourceInit(double sourceInit) {
        this.sourceInit = sourceInit;
    }
    public double reduceAmount(){
        return(sourceInit - amount);
    }

    public void setCreditAmt(double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public void setDebitAmt(double debitAmt) {
        this.debitAmt = debitAmt;
    }

    public double getCreditAmt() {
        return creditAmt;
    }

    public double getDebitAmt() {
        return debitAmt;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public void setDestinationNum(String destinationNum) {
        this.destinationNum = destinationNum;
    }
    
    public void calculateAmounts(){
        creditAmt = creditAmt - amount;
        if(creditAmt<0){
            debitAmt = debitAmt + Math.abs(creditAmt);
            creditAmt = creditAmt + Math.abs(creditAmt);
        }
    }
    
}
