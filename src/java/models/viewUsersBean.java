/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.userDetails;

/**
 *
 * @author Shaan
 */
public class viewUsersBean implements Serializable{
    
    List <userDetails> userList = new ArrayList<>();
    
    public void addUser(String userID, String firstName, String lastName, String userEmail, String lastLogin) {
        userList.add(new userDetails(userID, firstName, lastName, userEmail, lastLogin));
    }

    public List<userDetails> getUserList() {
        return userList;
    }
    
}
