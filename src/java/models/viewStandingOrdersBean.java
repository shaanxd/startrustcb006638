/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.standingOrder;

/**
 *
 * @author Shaan
 */
public class viewStandingOrdersBean implements Serializable{
    
    List<standingOrder> orderList = new ArrayList<>();
    
    public void addOrder(String orderID, String sourceNum, String destNum, String destType, String startDate, String durationType, int orderDuration, double orderAmount){
        orderList.add(new standingOrder(orderID, sourceNum, destNum, destType, startDate, durationType, orderDuration, orderAmount));
    }

    public List<standingOrder> getOrderList() {
        return orderList;
    }
    
}
