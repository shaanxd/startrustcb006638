/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.paymentProfile;

/**
 *
 * @author Shaan
 */
public class viewPaymentProfilesBean implements Serializable{
    List <paymentProfile> paymentList = new ArrayList<>();
    
    public void addPayment(String profileID, String profileName, String profileType, String accountNum) {
        paymentList.add(new paymentProfile(profileID, profileName, profileType, accountNum));
    }

    public List<paymentProfile> getPaymentList() {
        return paymentList;
    }
    
}
