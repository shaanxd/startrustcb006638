/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.openPaymentBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "openPayment", urlPatterns = {"/openPayment"})
public class openPayment extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            String userID = (String) session.getAttribute("userID");
            openPaymentBean bean = new openPaymentBean();
            
            PreparedStatement ps;
            ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("select * from bankaccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addAccountList(rs.getString("accountID"),rs.getString("accountName"), rs.getString("accountNumber"),rs.getString("balanceAmount"));
                }
                
                ps = con.prepareStatement("select * from paymentprofile where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addProfileList(rs.getString("profileID"),rs.getString("profileName"), rs.getString("profileType"),rs.getString("accountNumber"));
                }
                
                ps = con.prepareStatement("select * from creditcard where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addCardList(rs.getString("cardType"), rs.getString("cardNumber"));
                }
                
                request.setAttribute("bean", bean);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(openPayment.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            rd = request.getRequestDispatcher("openPayment.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        openPaymentBean bean = null;
        
        String paymentType = request.getParameter("paymentType");
        
        if(paymentType.equals("Bill")){
            bean = new openPaymentBean(request.getParameter("sourceAccount"),request.getParameter("profileType"),Double.parseDouble(request.getParameter("paymentAmount")));
        }
        else{
            bean = new openPaymentBean(request.getParameter("sourceAccount"),request.getParameter("cardSelect"),Double.parseDouble(request.getParameter("paymentAmount")));
            bean.setPaymentType("Credit Card");
        }
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateString = sdf.format(date);
        PreparedStatement ps;
        ResultSet rs;
        try {            
                date = sdf.parse(dateString);
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM bankaccount WHERE accountNumber=?");
                ps.setString(1, bean.getSourceNum());
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    bean.setSourceInit(rs.getDouble("balanceAmount"));
                }
                ps = con.prepareStatement("UPDATE bankaccount SET balanceAmount=? WHERE accountNumber=?");
                ps.setDouble(1, bean.reduceAmount());
                ps.setString(2, bean.getSourceNum());
                ps.executeUpdate();
                
                if(paymentType.equals("Card")){
                    ps = con.prepareStatement("SELECT * FROM creditcard where cardNumber=?");
                    ps.setString(1, bean.getDestinationNum());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.setCreditAmt(rs.getDouble("creditAmount"));
                        bean.setDebitAmt(rs.getDouble("debitAmount"));
                    }
                    
                    bean.calculateAmounts();
                    ps = con.prepareStatement("UPDATE creditcard SET creditAmount=?,debitAmount=? WHERE cardNumber=?");
                    ps.setDouble(1, bean.getCreditAmt());
                    ps.setDouble(2, bean.getDebitAmt());
                    ps.setString(3, bean.getDestinationNum());
                    
                    ps.executeUpdate();
                }
                else{
                    ps=con.prepareStatement("SELECT * FROM paymentprofile where profileID=?");
                    ps.setString(1, bean.getDestinationNum());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.setPaymentType(rs.getString("profileType"));
                        bean.setDestinationNum(rs.getString("accountNumber"));
                    }
                }
                ps = con.prepareStatement("INSERT INTO transactionhistory (userID, sourceNum,destinationNum,transferAmt, transferType, transferDate) VALUES (?,?,?,?,?,?)");
                ps.setString(1, userID);
                ps.setString(2, bean.getSourceNum());
                ps.setString(3, bean.getDestinationNum());
                ps.setDouble(4, bean.getAmount());
                ps.setString(5, bean.getPaymentType());
                ps.setDate(6, new java.sql.Date(new Date().getTime()));
                
                ps.executeUpdate();
                request.setAttribute("successMsg", true);
        }
        catch (ParseException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(openPayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.doGet(request, response);
    }
}
