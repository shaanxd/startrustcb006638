/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "userMessages", urlPatterns = {"/userMessages"})
public class userMessages extends pageBase {
    
    RequestDispatcher messageDispatcher;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);      
            userMessagesBean bean = new userMessagesBean();        
            String userID = (String) session.getAttribute("userID");

            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from useraccount");
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    if(!rs.getString("userID").equals(userID)){
                        bean.addUser(rs.getString("userEmail"));
                    }
                }
                PreparedStatement ps2 = con.prepareStatement("select * from messages where senderID=?");
                ps2.setString(1, userID);
                ResultSet rs2 = ps2.executeQuery();
                
                while(rs2!=null&&rs2.next()){
                    String recipientEmail = null;
                    PreparedStatement ps3 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps3.setString(1, rs2.getString("recipientID"));
                    ResultSet rs3 = ps3.executeQuery();
                    if(rs3!=null&&rs3.next()){
                        recipientEmail = rs3.getString("userEmail");
                    }
                    bean.addInboxListItem(recipientEmail, rs2.getString("messageSubject"), rs2.getString("messageBody"), rs2.getString("messageDate"));
                }
                
                PreparedStatement ps4 = con.prepareStatement("select * from messages where recipientID=?");
                ps4.setString(1, userID);
                ResultSet rs4 = ps4.executeQuery();
                
                while(rs4!=null&&rs4.next()){
                    String senderEmail = null;
                    PreparedStatement ps5 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps5.setString(1, rs4.getString("senderID"));
                    ResultSet rs5 = ps5.executeQuery();
                    if(rs5!=null&&rs5.next()){
                        senderEmail = rs5.getString("userEmail");
                    }
                    bean.addOutBoxListItem(senderEmail, rs4.getString("messageSubject"),  rs4.getString("messageBody"), rs4.getString("messageDate"));
                }
                request.setAttribute("userList", bean);

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            messageDispatcher = request.getRequestDispatcher("userMessages.jsp");
            messageDispatcher.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        String userID = (String) session.getAttribute("userID");
        
        userMessagesBean bean = new userMessagesBean();
        
        bean.setRecipientID(request.getParameter("recipientName"));
        bean.setMessageSubject(request.getParameter("messageSubject"));
        bean.setMessageBody(request.getParameter("messageBody"));
        try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("SELECT * from useraccount where userEmail=?");
                ps.setString(1, request.getParameter("recipientName"));
                ResultSet rs = ps.executeQuery();
                if(rs!=null&&rs.next()){
                    bean.setRecipientID(rs.getString("userID"));
                }
                
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                
                PreparedStatement ps2 = con.prepareStatement("INSERT INTO messages (senderID,recipientID,messageSubject,messageBody,messageDate) VALUES (?,?,?,?,?)");
                ps2.setString(1,userID);
                ps2.setString(2,bean.getRecipientID());
                ps2.setString(3,bean.getMessageSubject());
                ps2.setString(4,bean.getMessageBody());
                ps2.setString(5,dateFormat.format(date));
                
                ps2.executeUpdate();

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            this.doGet(request, response);
    }

}
