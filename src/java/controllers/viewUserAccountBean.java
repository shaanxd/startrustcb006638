/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import models.baseModels.accountDetails;
import models.baseModels.transactionDetails;

/**
 *
 * @author Shaan
 */
public class viewUserAccountBean implements Serializable{
    
    private String userName,userEmail,firstName,middleName,lastName;
    private String identityNum, passportNum, profileImg;
    private String userAddress, userCity, zipCode, mobileNum, homeNum;
    
    List<transactionDetails> transactionList = new ArrayList<>();
    
    public void addTransaction(String sourceNum, String destinationNum, String transferType, String transferDate, double transferAmt) {
        transactionList.add(new transactionDetails(sourceNum, destinationNum, transferType, transferDate, transferAmt));
    }
    
    public List<transactionDetails> getTransactionList() {
        return transactionList;
    }
    
    List <accountDetails> accountList = new ArrayList<>();
    
    public void addAccount(String accountID, String name, String accountNumber, double accountBalance, String branchName, double interestGained, String accountType) {
        accountList.add(new accountDetails(accountID, name, accountNumber, accountBalance, branchName, interestGained, accountType));
    }

    public List<accountDetails> getAccountList() {
        return accountList;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentityNum() {
        return identityNum;
    }

    public void setIdentityNum(String identityNum) {
        this.identityNum = identityNum;
    }

    public String getPassportNum() {
        return passportNum;
    }

    public void setPassportNum(String passportNum) {
        this.passportNum = passportNum;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserCity() {
        return userCity;
    }

    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getHomeNum() {
        return homeNum;
    }

    public void setHomeNum(String homeNum) {
        this.homeNum = homeNum;
    }
    
    
    
}
