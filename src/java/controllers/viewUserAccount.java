/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "viewUserAccount", urlPatterns = {"/viewUserAccount"})
public class viewUserAccount extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        if(session.getAttribute("userID")!=null){
            String userID = request.getParameter("userID");
            viewUserAccountBean bean = new viewUserAccountBean();
            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("SELECT * FROM useraccount WHERE userID=?");
                ps.setString(1,userID);
                ResultSet rs = ps.executeQuery();

                if(rs!=null&&rs.next()){
                    bean.setUserName(rs.getString("userName"));
                    bean.setUserEmail(rs.getString("userEmail"));
                    bean.setFirstName(rs.getString("firstName"));
                    bean.setMiddleName(rs.getString("middleName"));
                    bean.setLastName(rs.getString("lastName"));
                    bean.setIdentityNum(rs.getString("identityNumber"));    
                    bean.setPassportNum(rs.getString("passportNumber"));
                    bean.setProfileImg(rs.getString("profileImg"));
                    bean.setUserAddress(rs.getString("userAddress"));
                    bean.setUserCity(rs.getString("userCity"));
                    bean.setZipCode(rs.getString("zipCode"));
                    bean.setMobileNum(rs.getString("mobileNumber"));
                    bean.setHomeNum(rs.getString("homeNumber"));
                }
                
                ps = con.prepareStatement("SELECT * FROM transactionhistory WHERE userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    bean.addTransaction(rs.getString("sourceNum"), rs.getString("destinationNum"), rs.getString("transferType"), rs.getString("transferDate"), rs.getDouble("transferAmt"));
                }
                
                ps = con.prepareStatement("SELECT * FROM bankaccount WHERE userID=?");
                ps.setString(1, userID);
                rs=ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    bean.addAccount(rs.getString("accountID"), rs.getString("accountName"), rs.getString("accountNumber"), rs.getDouble("balanceAmount"), rs.getString("branchName"), rs.getDouble("interestGained"), rs.getString("accountType"));
                }
                
                request.setAttribute("bean", bean);
                
                super.doGet(request, response);
                rd = request.getRequestDispatcher("viewUserAccount.jsp");
                rd.forward(request, response);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(viewUserAccount.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            response.sendRedirect("login");
        }
    }
            
}
