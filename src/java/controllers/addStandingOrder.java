/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.addStandingOrderBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "addStandingOrder", urlPatterns = {"/addStandingOrder"})
public class addStandingOrder extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            String userID = (String) session.getAttribute("userID");
            addStandingOrderBean bean = new addStandingOrderBean();
            PreparedStatement ps; ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("select * from bankaccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addAccountList(rs.getString("accountID"),rs.getString("accountName"), rs.getString("accountNumber"),rs.getString("balanceAmount"));
                }
                
                ps = con.prepareStatement("select * from paymentprofile where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addProfileList(rs.getString("profileID"),rs.getString("profileName"), rs.getString("profileType"),rs.getString("accountNumber"));
                }
                
                ps = con.prepareStatement("select * from creditcard where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addCardList(rs.getString("cardType"), rs.getString("cardNumber"));
                }
                
                ps = con.prepareStatement("select * from beneficiaryprofile where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addBenList(rs.getString("benID"), rs.getString("benNumber"), rs.getString("benName"),rs.getString("benType"));
                }
                request.setAttribute("bean", bean);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(addStandingOrder.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            rd = request.getRequestDispatcher("addStandingOrder.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        PreparedStatement ps;
        ResultSet rs;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            addStandingOrderBean bean = new addStandingOrderBean(request.getParameter("sourceAccount"),request.getParameter("durationType"),Double.parseDouble(request.getParameter("orderAmount")),sdf.parse(request.getParameter("startFrom")),request.getParameter("orderDuration"));
            
            String orderType = request.getParameter("orderType");
            if(orderType.equals("beneficiary")){
                
                ps = con.prepareStatement("SELECT * FROM beneficiaryprofile where benID=?");
                ps.setString(1, request.getParameter("beneficiaryProfile"));
                rs = ps.executeQuery();

                if(rs!=null&&rs.next()){
                    bean.setDestNum(rs.getString("benNumber"));
                    bean.setDestType(rs.getString("benType"));
                }
            }
            else if(orderType.equals("payment")){
                
                ps = con.prepareStatement("SELECT * FROM paymentprofile where profileID=?");
                ps.setString(1, request.getParameter("paymentProfile"));
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    bean.setDestNum(rs.getString("accountNumber"));
                    bean.setDestType(rs.getString("profileType"));
                }
            }
            else{
                bean.setDestNum(request.getParameter("cardProfile"));
                bean.setDestType("Credit Card");
            }
            
            ps = con.prepareStatement("INSERT INTO standingorder (userID, sourceNum, destNum, destType, orderAmount, startDate, orderDuration, durationType) VALUES (?,?,?,?,?,?,?,?)");
            ps.setString(1, userID);
            ps.setString(2, bean.getSourceNum());
            ps.setString(3, bean.getDestNum());
            ps.setString(4, bean.getDestType());
            ps.setDouble(5, bean.getOrderAmt());
            ps.setDate(6, new java.sql.Date(bean.getStartDate().getTime()));
            ps.setString(7, bean.getDuration());
            ps.setString(8, bean.getDurType());
            
            ps.executeUpdate();
            
            request.setAttribute("successMsg", true);
            
        } catch (ParseException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(addStandingOrder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.doGet(request, response);
    }
    
}
