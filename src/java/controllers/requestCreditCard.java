/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.activityLogBean;
import models.requestCreditCardBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "requestCreditCard", urlPatterns = {"/requestCreditCard"})
public class requestCreditCard extends pageBase {
    
    RequestDispatcher creditCardDispatcher;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            requestCreditCardBean bean = new requestCreditCardBean();
            String userID = (String) session.getAttribute("userID");

            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from bankaccount where userID=?");
                ps.setString(1, userID);
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addAccountList(rs.getString("accountID"),rs.getString("accountName"), rs.getString("accountNumber"),rs.getString("balanceAmount"));
                }
                request.setAttribute("accountList", bean);

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            creditCardDispatcher = request.getRequestDispatcher("requestCreditCard.jsp");
            creditCardDispatcher.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       HttpSession session = request.getSession();
       String userID = (String) session.getAttribute("userID");
       
       requestCreditCardBean bean = new requestCreditCardBean(request.getParameter("sourceAccount"),request.getParameter("cardType"),request.getParameter("creditLimit"));
       
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        
       try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();            
            
            PreparedStatement ps = con.prepareStatement("insert into cardrequest (userID,accountID,cardType,creditLimit,requestDate) VALUES (?,?,?,?,?)");
            ps.setString(1, userID);
            ps.setString(2, bean.getAccountID() );
            ps.setString(3, bean.getCardType());
            ps.setString(4, bean.getCreditLimit());
            ps.setString(5, dateFormat.format(date));
            ps.executeUpdate();
            
            activityLogBean bean2 = new activityLogBean("Card Request");
            bean2.setCardType(bean.getCardType());
            bean2.setActivityDescription();
            
            PreparedStatement ps2 = con.prepareStatement("insert into activitylog (userID,activityType,activityDescription,activityDate) VALUES (?,?,?,?)");
            ps2.setString(1, userID);
            ps2.setString(2, bean2.getActivityType());
            ps2.setString(3, bean2.getActivityDescription());
            ps2.setString(4, dateFormat.format(date));
            
            ps2.executeUpdate();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(openAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("successMessage", true);
        this.doGet(request, response);
    }
}
