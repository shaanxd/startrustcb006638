/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;
import utilities.passwordEncryption;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "registerAccount", urlPatterns = {"/registerAccount"})
public class registerAccount extends pageBase {

    RequestDispatcher rd;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            rd = request.getRequestDispatcher("registerAccount.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            registerAccountBean bean = new registerAccountBean(
                    request.getParameter("username"),
                    request.getParameter("firstName"),
                    request.getParameter("middleName"),
                    request.getParameter("lastName"),
                    request.getParameter("emailAddress"),
                    request.getParameter("identityNumber"),
                    request.getParameter("passportNumber"),
                    request.getParameter("gender"),
                    request.getParameter("address"),
                    request.getParameter("city"),
                    request.getParameter("zipCode"),
                    request.getParameter("mobileNumber"),
                    request.getParameter("homeNumber"),
                    request.getParameter("accountNumber"),
                    request.getParameter("accountType"),
                    request.getParameter("branchName"),
                    Double.parseDouble(request.getParameter("initialAmount")),
                    sdf.parse(request.getParameter("dateOfBirth")),
                    request.getParameter("accountName")
            );
            
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("select * from useraccount where userEmail=?");
            ps.setString(1, bean.getEmail());
            ResultSet rs = ps.executeQuery();
            if(rs!=null&&rs.next()){
                request.setAttribute("emailError",true);
                this.doGet(request, response);
            }
            ps = con.prepareStatement("select * from useraccount where userName=?");
            ps.setString(1, bean.getUsername());
            rs = ps.executeQuery();
            
            if(rs!=null&&rs.next()){
                request.setAttribute("usernameError", true);
                this.doGet(request, response);
            }
            else{
                ps = con.prepareStatement("select * from bankaccount where accountNumber=?");
                ps.setString(1, bean.getAccountNum());
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    request.setAttribute("accountNumError",true);
                    this.doGet(request, response);
                }
                else{
                    ps = con.prepareStatement("insert into useraccount (userName,userEmail,userType,"
                            + "firstName,middleName,lastName,dateOfBirth,identityNumber,passportNumber,"
                            + "userGender,userAddress,userCity,zipCode,mobileNumber,homeNumber,userPassword) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    ps.setString(1, bean.getUsername());
                    ps.setString(2, bean.getEmail());
                    ps.setString(3, "customer");
                    ps.setString(4, bean.getFirstname());
                    ps.setString(5, bean.getMiddlename());
                    ps.setString(6, bean.getLastname());
                    ps.setDate(7, new java.sql.Date(bean.getDateOfBirth().getTime()));
                    ps.setString(8, bean.getIdNum());
                    ps.setString(9, bean.getPassportNum());
                    ps.setString(10, bean.getGender());
                    ps.setString(11, bean.getAddress());
                    ps.setString(12, bean.getCity());
                    ps.setString(13, bean.getZipCode());
                    ps.setString(14, bean.getMobileNum());
                    ps.setString(15, bean.getHomeNum());
                    ps.setString(16, passwordEncryption.encrypt("customer123"));
                    ps.executeUpdate();

                    ps = con.prepareStatement("select * from useraccount where userEmail=?");
                    ps.setString(1, bean.getEmail());
                    rs = ps.executeQuery();
                    if(rs!=null&&rs.next()){
                        bean.setUserID(rs.getString("userID"));
                    }
                    ps = con.prepareStatement("insert into bankaccount (userID,accountNumber,accountName,accountType,branchName,balanceAmount) values (?,?,?,?,?,?)");
                    ps.setString(1, bean.getUserID());
                    ps.setString(2, bean.getAccountNum());
                    ps.setString(3, bean.getAccountName());
                    ps.setString(4, bean.getAccountType());
                    ps.setString(5, bean.getAccountBranch());
                    ps.setDouble(6, bean.getInitialDeposit());

                    ps.executeUpdate();
                    request.setAttribute("successMessage", true);
                }
            }
            
        } catch (ParseException | ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
            Logger.getLogger(registerAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        super.doGet(request, response);
        this.doGet(request, response);
    }

}
