/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.adminDashboardBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "adminDashboard", urlPatterns = {"/adminDashboard"})
public class adminDashboard extends pageBase {
     
    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            String userID = (String)session.getAttribute("userID");
            adminDashboardBean bean = new adminDashboardBean();
            PreparedStatement ps; ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM accounttype");
                rs = ps.executeQuery();
                while(rs!=null&&rs.next()){
                    switch(rs.getString("accountType")){
                        case "Savings": bean.setSavingsRate(rs.getString("interestRate"));break;
                        case "Current": bean.setCurrentRate(rs.getString("interestRate"));break;
                        case "Fixed Deposit": bean.setFdRate(rs.getString("interestRate"));break;
                    }
                }
                
                ps = con.prepareStatement("select * from messages where recipientID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    String recipientEmail = null;
                    PreparedStatement ps2 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps2.setString(1, rs.getString("senderID"));
                    ResultSet rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        recipientEmail = rs2.getString("userEmail");
                    }
                    bean.addInboxListItem(recipientEmail, rs.getString("messageSubject"), rs.getString("messageBody"), rs.getString("messageDate"));
                }
                request.setAttribute("bean", bean);
                super.doGet(request, response);
                rd = request.getRequestDispatcher("adminDashboard.jsp");
                rd.forward(request, response);
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(adminDashboard.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
