/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.interestRateBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "verifyInterest", urlPatterns = {"/verifyInterest"})
public class verifyInterest extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            
            rd= request.getRequestDispatcher("verifyInterest.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        interestRateBean bean = new interestRateBean(Double.parseDouble(request.getParameter("interestRate")),request.getParameter("accountType"));
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE accounttype SET interestRate=? WHERE accountType=?");
            ps.setDouble(1, bean.getInterestRate());
            ps.setString(2, bean.getAccountType());
            ps.executeUpdate();
            
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(verifyInterest.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.doGet(request, response);
    }
}
