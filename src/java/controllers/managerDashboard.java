/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.managerDashboardBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "managerDashboard", urlPatterns = {"/managerDashboard"})
public class managerDashboard extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        if(session.getAttribute("userID")!=null){
            String userID = (String)session.getAttribute("userID");
            
            managerDashboardBean bean = new managerDashboardBean();
            PreparedStatement ps,ps2; 
            ResultSet rs,rs2;
            String username = null;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("select * from messages where recipientID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    String recipientEmail = null;
                    ps2 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps2.setString(1, rs.getString("senderID"));
                    rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        recipientEmail = rs2.getString("userEmail");
                    }
                    bean.addInboxListItem(recipientEmail, rs.getString("messageSubject"), rs.getString("messageBody"), rs.getString("messageDate"));
                }
                
                ps = con.prepareStatement("select * from cardrequest");
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    ps2 = con.prepareStatement("select * from useraccount where userID=?");
                    ps2.setString(1, rs.getString("userID"));
                    rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        username = rs2.getString("userName");
                    }
                    bean.addRequest(username, rs.getString("cardType"),rs.getString("requestDate"));
                }
                
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(managerDashboard.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            request.setAttribute("bean", bean);
            rd = request.getRequestDispatcher("managerDashboard.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
