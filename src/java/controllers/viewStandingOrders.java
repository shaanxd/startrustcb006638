/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.viewStandingOrdersBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "viewStandingOrders", urlPatterns = {"/viewStandingOrders"})
public class viewStandingOrders extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            String userID = (String) session.getAttribute("userID");
            viewStandingOrdersBean bean = new viewStandingOrdersBean();
            PreparedStatement ps;
            ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM standingorder where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    bean.addOrder(rs.getString("orderID"), rs.getString("sourceNum"), rs.getString("destNum"), rs.getString("destType"), rs.getString("startDate"), rs.getString("durationType"), rs.getInt("orderDuration"), rs.getDouble("orderAmount"));
                }
                
                request.setAttribute("bean", bean);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(viewStandingOrders.class.getName()).log(Level.SEVERE, null, ex);
            }            
            super.doGet(request, response);
            rd = request.getRequestDispatcher("viewStandingOrders.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PreparedStatement ps;
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("DELETE FROM standingorder where orderID=?");
            ps.setString(1, request.getParameter("orderID"));
            ps.executeUpdate();

        }catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(viewStandingOrders.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.doGet(request, response);
    }
}
