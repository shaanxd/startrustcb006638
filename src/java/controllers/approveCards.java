/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "approveCards", urlPatterns = {"/approveCards"})
public class approveCards extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            String userID = (String) session.getAttribute("userID");
            approveCardsBean bean = new approveCardsBean();
            PreparedStatement ps2;
            ResultSet rs2;
            String username = null;
            
            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from cardrequest");
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    ps2 = con.prepareStatement("select * from useraccount where userID=?");
                    ps2.setString(1, rs.getString("userID"));
                    rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        username = rs2.getString("userName");
                    }
                    bean.addRequest(rs.getString("requestID"), username, rs.getString("cardType"), rs.getDouble("creditLimit"), rs.getString("requestDate"));
                }
                request.setAttribute("requestList", bean);

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            rd = request.getRequestDispatcher("approveCards.jsp");
            rd.forward(request, response);
            
            
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        String userID = (String)session.getAttribute("userID");
        approveCardsBean bean = new approveCardsBean();
        bean.setRequestID(request.getParameter("requestID"));
        
        PreparedStatement ps; ResultSet rs;
        String approvalType = request.getParameter("approvalType");
        if(approvalType.equals("Reject")){
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();            

                ps = con.prepareStatement("DELETE FROM cardrequest where requestID=?");
                ps.setString(1, bean.getRequestID());
                ps.executeUpdate();
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(approveAccounts.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            try{
                bean.setExpiryDate(request.getParameter("expiryDate"));
                bean.setCardNum(request.getParameter("cardNumber"));
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();            

                ps = con.prepareStatement("SELECT * FROM creditcard where cardNumber=?");
                ps.setString(1, bean.getCardNum());
                
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    request.setAttribute("cardExists", true);
                }
                else{
                    ps = con.prepareStatement("SELECT * FROM cardrequest where requestID=?");
                    ps.setString(1, bean.getRequestID());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.addInfo(rs.getString("accountID"), rs.getString("cardType"), rs.getDouble("creditLimit"),rs.getString("userID"));
                    }
                    
                    ps = con.prepareStatement("SELECT * FROM bankaccount where accountID=?");
                    ps.setString(1, bean.getAccountID());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.setBalanceAmount(rs.getDouble("balanceAmount"));
                    }
                    if(!bean.checkBalance()){
                        request.setAttribute("amtError", true);
                    }
                    else{
                        ps = con.prepareStatement("UPDATE bankaccount SET balanceAmount=? where accountID=?");
                        ps.setDouble(1, bean.calcBalance());
                        ps.setString(2, bean.getAccountID());
                        ps.executeUpdate();
                        
                        ps = con.prepareStatement("INSERT INTO creditcard (userID,cardNumber,cardType,creditLimit,expiryDate,creditAmount,debitAmount) VALUES (?,?,?,?,?,?,?)");
                        ps.setString(1, bean.getUserID());
                        ps.setString(2, bean.getCardNum());
                        ps.setString(3, bean.getCardType());
                        ps.setDouble(4, bean.getCreditLimit());
                        ps.setString(5, bean.getExpiryDate());
                        ps.setDouble(6, 0);
                        ps.setDouble(7, 0);
                        
                        ps.executeUpdate();
                        
                        ps = con.prepareStatement("DELETE FROM cardrequest where requestID=?");
                        ps.setString(1, bean.getRequestID());
                        ps.executeUpdate();
                        request.setAttribute("successMsg",true);
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(approveCards.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.doGet(request, response);
    }
    
}
