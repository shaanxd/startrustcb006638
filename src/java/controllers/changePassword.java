/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilities.databaseConnection;
import utilities.passwordEncryption;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "changePassword", urlPatterns = {"/changePassword"})
public class changePassword extends HttpServlet {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            rd = request.getRequestDispatcher("changePassword.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        String userType = (String)session.getAttribute("userType");
        
        String password = request.getParameter("password");
        
        try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps2 = con.prepareStatement("UPDATE useraccount SET userPassword=? where userID=?");
                ps2.setString(1, passwordEncryption.encrypt(password));
                ps2.setString(2, userID);
                ps2.executeUpdate();
                switch(userType){
                    case "customer": response.sendRedirect("userDashboard"); break;
                    case "admin": response.sendRedirect("adminDashboard"); break;
                    case "manager": response.sendRedirect("managerDashboard"); break;
                }
        } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
            Logger.getLogger(changePassword.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
