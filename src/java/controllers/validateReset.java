/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.resetPasswordBean;
import utilities.databaseConnection;
import utilities.passwordEncryption;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "validateReset", urlPatterns = {"/validateReset"})
public class validateReset extends HttpServlet {
    
    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("resetID")!=null){
            rd = request.getRequestDispatcher("validateReset.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("resetPassword");
        }
    }

   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        resetPasswordBean bean = new resetPasswordBean();
        
        bean.setResetID((String) session.getAttribute("resetID"));
        bean.setPassword(request.getParameter("password"));
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM resetpassword WHERE resetID=?");
            ps.setString(1, bean.getResetID());
            ResultSet rs = ps.executeQuery();
            
            if(rs!=null&&rs.next()){
                if(rs.getString("tempPassword").equals(passwordEncryption.encrypt(bean.getPassword()))){
                    PreparedStatement ps2 = con.prepareStatement("DELETE FROM resetpassword where resetID=?");
                    ps2.setString(1, bean.getResetID());
                    ps2.executeUpdate();
                    
                    session.invalidate();
                    
                    session = request.getSession(true);
                    session.setAttribute("userID", rs.getString("userID"));
                    
                    ps2 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps2.setString(1, rs.getString("userID"));
                    ResultSet rs2 = ps2.executeQuery();
                    
                    if(rs2!=null&&rs2.next()){
                        session.setAttribute("userType", rs2.getString("userType"));
                        response.sendRedirect("changePassword");
                    }
                 }
                else{
                    request.setAttribute("errorMessage", true);
                    this.doGet(request, response);
                }
            }
        } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
            Logger.getLogger(validateReset.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
