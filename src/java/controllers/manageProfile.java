/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.*;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "manageProfile", urlPatterns = {"/manageProfile"})
public class manageProfile extends pageBase {

    RequestDispatcher mpDispatcher;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();  
        if(session.getAttribute("userID")==null){
            response.sendRedirect("login");
        }
        else{
            super.doGet(request, response);      
            manageProfileBean bean = new manageProfileBean();        
            String userID = (String) session.getAttribute("userID");

            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from useraccount where userID=?");
                ps.setString(1,userID);
                ResultSet rs = ps.executeQuery();

                if(rs!=null&&rs.next()){
                    bean.setUserName(rs.getString("userName"));
                    bean.setUserEmail(rs.getString("userEmail"));
                    bean.setFirstName(rs.getString("firstName"));
                    bean.setMiddleName(rs.getString("middleName"));
                    bean.setLastName(rs.getString("lastName"));
                    bean.setIdentityNum(rs.getString("identityNumber"));    
                    bean.setPassportNum(rs.getString("passportNumber"));
                    bean.setProfileImg(rs.getString("profileImg"));
                    bean.setUserAddress(rs.getString("userAddress"));
                    bean.setUserCity(rs.getString("userCity"));
                    bean.setZipCode(rs.getString("zipCode"));
                    bean.setMobileNum(rs.getString("mobileNumber"));
                    bean.setHomeNum(rs.getString("homeNumber"));
                }
                request.setAttribute("profileBean", bean);

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            mpDispatcher = request.getRequestDispatcher("manageProfile.jsp");
            mpDispatcher.forward(request, response);
        }
        
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")==null){
            response.sendRedirect("login");
        }
        else{
        
        String userID = (String) session.getAttribute("userID");
        String userType = (String) session.getAttribute("userType");
        
        manageProfileBean bean = new manageProfileBean();
        
        bean.setUserAddress(request.getParameter("address"));
        bean.setUserCity(request.getParameter("city"));
        bean.setZipCode(request.getParameter("zipCode"));
        bean.setMobileNum(request.getParameter("mobileNumber"));
        bean.setHomeNum(request.getParameter("homeNumber"));
        
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        
        try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                               
                PreparedStatement ps = con.prepareStatement("UPDATE useraccount SET userAddress=?, userCity=?, zipCode=?, mobileNumber=?, homeNumber=? where userID=?");
                ps.setString(1, bean.getUserAddress());
                ps.setString(2,bean.getUserCity());
                ps.setString(3,bean.getZipCode());
                ps.setString(4,bean.getMobileNum());
                ps.setString(5,bean.getHomeNum());
                ps.setString(6,userID);
                ps.executeUpdate();
                
                if(!(request.getParameter("password").isEmpty())){
                    bean.setPassword(passwordEncryption.encrypt(request.getParameter("password")));
                    PreparedStatement ps2 = con.prepareStatement("UPDATE useraccount SET userPassword=? where userID=?");
                    ps2.setString(1, bean.getPassword());
                    ps2.setString(2, userID);
                    ps2.executeUpdate();
                }
                if(userType.equals("customer")){
                    activityLogBean bean2 = new activityLogBean("Updated Profile");
                    bean2.setActivityDescription();

                    ps = con.prepareStatement("insert into activitylog (userID,activityType,activityDescription,activityDate) VALUES (?,?,?,?)");
                    ps.setString(1, userID);
                    ps.setString(2, bean2.getActivityType());
                    ps.setString(3, bean2.getActivityDescription());
                    ps.setString(4, dateFormat.format(date));
                    ps.executeUpdate();
                }
            } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("successMessage", true);
            this.doGet(request, response);
        }
    }

}
