/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "transferFunds", urlPatterns = {"/transferFunds"})
public class transferFunds extends pageBase {
    RequestDispatcher rd;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            
            String userID = (String)session.getAttribute("userID");
            transferFundsBean bean = new transferFundsBean();
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from beneficiaryprofile where userID=?");
                ps.setString(1, userID);
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    if(rs.getString("benType").equals("Startrust")){
                        bean.addSt(rs.getString("benID"), rs.getString("benNumber"), rs.getString("benName"));
                    }
                    else{
                        bean.addExt(rs.getString("benID"), rs.getString("benNumber"), rs.getString("benName"), rs.getString("benType"));
                    }
                }
                ps = con.prepareStatement("select * from bankaccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addAccountList(rs.getString("accountID"),rs.getString("accountName"), rs.getString("accountNumber"),rs.getString("balanceAmount"));
                }
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(transferFunds.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("bean", bean);
            super.doGet(request, response);
            rd = request.getRequestDispatcher("transferFunds.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        
        transferFundsBean bean = new transferFundsBean();
        
        bean.setSourceAcctNum(request.getParameter("sourceAccount"));
        bean.setDeductAmount(Double.parseDouble(request.getParameter("paymentAmount")));
        
        String transferType = request.getParameter("accountType");
        
        if(transferType.equals("Own")){
            bean.setDestAcctNum(request.getParameter("ownAccount"));
        }
        else if(transferType.equals("Startrust")){
            bean.setDestAcctNum(request.getParameter("stBeneficiary"));
        }
        else{
            bean.setDestAcctNum(request.getParameter("otherBeneficiary"));
        }
        
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        
        PreparedStatement ps;
        ResultSet rs;
        try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM bankaccount WHERE accountNumber=?");
                ps.setString(1, bean.getSourceAcctNum());
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    bean.setSourceInitialAmount(rs.getDouble("balanceAmount"));
                }
                
                ps = con.prepareStatement("UPDATE bankaccount SET balanceAmount=? WHERE accountNumber=?");
                ps.setDouble(1, bean.reduceAmount());
                ps.setString(2, bean.getSourceAcctNum());
                ps.executeUpdate();
                
                if(transferType.equals("Own")||transferType.equals("Startrust")){
                    ps = con.prepareStatement("SELECT * FROM bankaccount WHERE accountNumber=?");
                    ps.setString(1, bean.getDestAcctNum());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.setDestInitialAmount(rs.getDouble("balanceAmount"));
                    }
                    
                    ps = con.prepareStatement("UPDATE bankaccount SET balanceAmount=? WHERE accountNumber=?");
                    ps.setDouble(1, bean.addAmount());
                    ps.setString(2, bean.getDestAcctNum());
                    ps.executeUpdate();
                }
                
                ps = con.prepareStatement("INSERT INTO transactionhistory (userID,sourceNum,destinationNum,transferAmt,transferDate,transferType) VALUES (?,?,?,?,?,?)");
                ps.setString(1, userID);
                ps.setString(2, bean.getSourceAcctNum());
                ps.setString(3, bean.getDestAcctNum());
                ps.setDouble(4, bean.getDeductAmount());
                ps.setString(5, sdf.format(date));
                ps.setString(6, transferType);
                
                ps.executeUpdate();
                request.setAttribute("successMsg", true);
                
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(transferFunds.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.doGet(request, response);
    }
}
