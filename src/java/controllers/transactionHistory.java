/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.transactionHistoryBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "transactionHistory", urlPatterns = {"/transactionHistory"})
public class transactionHistory extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            rd = request.getRequestDispatcher("transactionHistory.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        String userID = (String)session.getAttribute("userID");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            transactionHistoryBean bean = new transactionHistoryBean(sdf.parse(request.getParameter("fromDate")),sdf.parse(request.getParameter("toDate")));
            
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("select * from transactionhistory where userID=? and transferDate>=? and transferDate<=?");
            ps.setString(1, userID);
            ps.setDate(2,new java.sql.Date(bean.getFromDate().getTime()));
            ps.setDate(3,new java.sql.Date(bean.getToDate().getTime()));
            ResultSet rs = ps.executeQuery();

            while(rs!=null&&rs.next()){
                bean.addTransaction(rs.getString("sourceNum"), rs.getString("destinationNum"), rs.getString("transferType"), rs.getString("transferDate"), rs.getDouble("transferAmt"));
            }

            request.setAttribute("transactionBean", bean);
        } catch (ParseException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(transactionHistory.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.doGet(request, response);
    }
}
