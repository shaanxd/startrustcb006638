/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.*;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "addPaymentProfile", urlPatterns = {"/addPaymentProfile"})
public class addPaymentProfile extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            rd = request.getRequestDispatcher("addPaymentProfile.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        
        addPaymentProfileBean bean = new addPaymentProfileBean(request.getParameter("profileName"),request.getParameter("accountNo"),request.getParameter("profileType"));
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();            
            
            PreparedStatement ps = con.prepareStatement("insert into paymentprofile (userID,profileName,profileType,accountNumber) VALUES (?,?,?,?)");
            ps.setString(1, userID);
            ps.setString(2, bean.getProfileName() );
            ps.setString(3, bean.getProfileType());
            ps.setString(4, bean.getAccountNumber());
            ps.executeUpdate();
            
            activityLogBean bean2 = new activityLogBean("Payment Profile");
            bean2.setActivityDescription();
            
            PreparedStatement ps2 = con.prepareStatement("insert into activitylog (userID,activityType,activityDescription,activityDate) VALUES (?,?,?,?)");
            ps2.setString(1, userID);
            ps2.setString(2, bean2.getActivityType());
            ps2.setString(3, bean2.getActivityDescription());
            ps2.setString(4, dateFormat.format(date));
            
            ps2.executeUpdate();
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(openAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("successMessage", true);
        this.doGet(request, response);
    }
}
