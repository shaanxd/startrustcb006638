/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.resetPasswordBean;
import utilities.databaseConnection;
import utilities.passwordEncryption;
import utilities.sendMail;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "resetPassword", urlPatterns = {"/resetPassword"})
public class resetPassword extends HttpServlet {
    
    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        rd = request.getRequestDispatcher("resetPassword.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userEmail = request.getParameter("userEmail");
        resetPasswordBean bean = new resetPasswordBean();
        try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from useraccount where userEmail=?");
                ps.setString(1, userEmail);
                ResultSet rs = ps.executeQuery();

                if(rs!=null && rs.next())
                {
                    bean.setUserID(rs.getString("userID"));
                    PreparedStatement ps2 = con.prepareStatement("DELETE FROM resetpassword where userID=?");
                    ps2.setString(1, bean.getUserID());
                    ps2.executeUpdate();
                    
                    bean.setPassword();
                    
                    ps2 = con.prepareStatement("INSERT INTO resetpassword (userID,tempPassword) VALUES (?,?)");
                    ps2.setString(1, bean.getUserID());
                    ps2.setString(2, passwordEncryption.encrypt(bean.getPassword()));
                    ps2.executeUpdate();
                    
                    ps2 = con.prepareStatement("SELECT * FROM resetpassword where userID=?");
                    ps2.setString(1, bean.getUserID());
                    ResultSet rs2 = ps2.executeQuery();
                    
                    if(rs2!=null&&rs2.next()){
                        sendMail.sendMail(userEmail, "Reset Password", "Temporary Password : "+bean.getPassword());
                        session.setAttribute("resetID", rs2.getString("resetID"));
                        response.sendRedirect("validateReset");
                    }
                }
                else
                {
                    request.setAttribute("errorMessage",true);
                    this.doGet(request, response);
                }
            } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
            Logger.getLogger(validateReset.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
