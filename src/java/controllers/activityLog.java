/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.activityLogBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "activityLog", urlPatterns = {"/activityLog"})
public class activityLog extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            activityLogBean bean = new activityLogBean();
            
            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from useraccount where userType=?");
                ps.setString(1, "customer");
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addUser(rs.getString("userID"),rs.getString("userName"));
                }
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(activityLog.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            request.setAttribute("bean",bean);
            rd = request.getRequestDispatcher("activityLog.jsp");
            rd.forward( request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        activityLogBean activityBean = new activityLogBean();
        activityBean.setUserID(request.getParameter("userAccount"));
        
        try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from activitylog where userID=?");
                ps.setString(1, activityBean.getUserID());
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    activityBean.addActivity(rs.getString("activityType"),rs.getString("activityDescription"),rs.getString("activityDate"));
                }
                
                request.setAttribute("activityList", activityBean);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(activityLog.class.getName()).log(Level.SEVERE, null, ex);
            }
        this.doGet(request, response);
    }
}
