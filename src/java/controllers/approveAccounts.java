/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "approveAccounts", urlPatterns = {"/approveAccounts"})
public class approveAccounts extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            approveAccountsBean bean = new approveAccountsBean();
            PreparedStatement ps2;
            ResultSet rs2;
            String username = null;
            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                PreparedStatement ps = con.prepareStatement("select * from accountrequest");
                ResultSet rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    ps2 = con.prepareStatement("select * from useraccount where userID=?");
                    ps2.setString(1, rs.getString("userID"));
                    rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        username = rs2.getString("userName");
                    }
                    bean.addRequest(rs.getString("requestID"), username, rs.getString("requestDate"), rs.getString("accountType"), rs.getString("branchName"));
                }
                request.setAttribute("requestList", bean);

            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
            }
            rd = request.getRequestDispatcher("approveAccounts.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        String decisionType = request.getParameter("approvalType");
        approveAccountsBean bean = new approveAccountsBean();
        bean.setRequestID(request.getParameter("requestID"));
        PreparedStatement ps;
        ResultSet rs;
        
        if(decisionType.equals("Reject")){
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();            

                ps = con.prepareStatement("DELETE FROM accountrequest where requestID=?");
                ps.setString(1, bean.getRequestID());
                ps.executeUpdate();
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(approveAccounts.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            try{
                bean.setAccountNum(request.getParameter("accountNumber"));
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();            

                ps = con.prepareStatement("SELECT * FROM bankaccount where accountNumber=?");
                ps.setString(1, bean.getAccountNum());
                rs = ps.executeQuery();
                
                if(rs!=null&&rs.next()){
                    request.setAttribute("accExist", true);
                }
                else{
                    ps = con.prepareStatement("SELECT * FROM accountrequest where requestID=?");
                    ps.setString(1, bean.getRequestID());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.addInfo(rs.getString("sourceAccountID"), rs.getString("userID"), rs.getString("accountType"), rs.getString("accountName"), rs.getString("branchName"), rs.getDouble("initialDeposit"));
                    }
                    
                    ps = con.prepareStatement("SELECT * FROM bankaccount where accountID=?");
                    ps.setString(1, bean.getFeeAccount());
                    rs = ps.executeQuery();
                    
                    if(rs!=null&&rs.next()){
                        bean.setSourceAccAmount(rs.getDouble("balanceAmount"));
                    }
                    
                    if(bean.compareVals()){
                        request.setAttribute("amtError", true);
                    }
                    else{
                        ps = con.prepareStatement("UPDATE bankaccount SET balanceAmount=? where accountID=?");
                        ps.setDouble(1, bean.calcAmount());
                        ps.setString(2, bean.getFeeAccount());
                        ps.executeUpdate();
                        
                        ps = con.prepareStatement("INSERT INTO bankaccount (userID, accountNumber, accountName, accountType, branchName, balanceAmount) VALUES (?,?,?,?,?,?)");
                        ps.setString(1, bean.getUserID());
                        ps.setString(2, bean.getAccountNum());
                        ps.setString(3, bean.getAccountName());
                        ps.setString(4, bean.getAccountType());
                        ps.setString(5, bean.getBranchName());
                        ps.setDouble(6, bean.getInitDeposit());
                        ps.executeUpdate();
                        
                        ps = con.prepareStatement("DELETE FROM accountrequest where requestID=?");
                        ps.setString(1, bean.getRequestID());
                        ps.executeUpdate();
                        request.setAttribute("successMsg", true);
                    }
                }
            } catch (SQLException | ClassNotFoundException ex) {
                Logger.getLogger(approveAccounts.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.doGet(request, response);
    }
}
