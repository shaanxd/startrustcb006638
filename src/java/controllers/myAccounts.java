/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.myAccountsBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "myAccounts", urlPatterns = {"/myAccounts"})
public class myAccounts extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        if(session.getAttribute("userID")!=null){
            String userID = (String) session.getAttribute("userID");
            myAccountsBean bean = new myAccountsBean();
            PreparedStatement ps; ResultSet rs;
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM bankaccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                while(rs!=null&&rs.next()){
                    bean.addAccount(rs.getString("accountID"), rs.getString("accountName"), rs.getString("accountNumber"), rs.getDouble("balanceAmount"), rs.getString("branchName"), rs.getDouble("interestGained"), rs.getString("accountType"));
                }
                request.setAttribute("bean", bean);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(viewPaymentProfiles.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            rd = request.getRequestDispatcher("myAccounts.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
