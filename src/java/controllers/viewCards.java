/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.viewCardsBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "viewCards", urlPatterns = {"/viewCards"})
public class viewCards extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            String userID = (String)session.getAttribute("userID");
            viewCardsBean bean = new viewCardsBean();
            SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
            PreparedStatement ps;
            ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM creditcard where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    bean.addCard(rs.getString("cardType"), rs.getString("cardNumber"), sdf.format(rs.getDate("expiryDate")), rs.getDouble("creditAmount"), rs.getDouble("debitAmount"),rs.getDouble("creditLimit"));
                }
                
                ps = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                if(rs!=null&&rs.next()){
                    bean.setName(rs.getString("firstName"), rs.getString("lastName"));
                }
                request.setAttribute("bean",bean);
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(viewCards.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            rd = request.getRequestDispatcher("viewCards.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
