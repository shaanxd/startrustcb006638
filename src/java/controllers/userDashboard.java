/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.userDashboardBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "userDashboard", urlPatterns = {"/userDashboard"})
public class userDashboard extends pageBase {
    RequestDispatcher dashboardDispatcher;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            long x = 24 * 60 * 60 * 1000;
            Date current = new Date();
            Date fromDate = new Date(current.getTime() - 7 * x);
            String userID = (String)session.getAttribute("userID");
            userDashboardBean bean = new userDashboardBean();
            ResultSet rs; PreparedStatement ps;
            try {            
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("select * from messages where recipientID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                
                while(rs!=null&&rs.next()){
                    String recipientEmail = null;
                    PreparedStatement ps2 = con.prepareStatement("SELECT * FROM useraccount where userID=?");
                    ps2.setString(1, rs.getString("senderID"));
                    ResultSet rs2 = ps2.executeQuery();
                    if(rs2!=null&&rs2.next()){
                        recipientEmail = rs2.getString("userEmail");
                    }
                    bean.addInboxListItem(recipientEmail, rs.getString("messageSubject"), rs.getString("messageBody"), rs.getString("messageDate"));
                }
                
                ps = con.prepareStatement("SELECT * FROM bankaccount where userID=?");
                ps.setString(1, userID);
                rs = ps.executeQuery();
                while(rs!=null&&rs.next()){
                    bean.addAccount(rs.getString("accountID"), rs.getString("accountName"), rs.getString("accountNumber"), rs.getDouble("balanceAmount"));
                }
                
                ps = con.prepareStatement("select * from transactionhistory where userID=? and transferDate>=? and transferDate<=?");
                ps.setString(1, userID);
                ps.setDate(2,new java.sql.Date(fromDate.getTime()));
                ps.setDate(3,new java.sql.Date(current.getTime()));
                rs = ps.executeQuery();

                while(rs!=null&&rs.next()){
                    bean.addTransaction(rs.getString("sourceNum"), rs.getString("destinationNum"), rs.getString("transferType"), rs.getString("transferDate"), rs.getDouble("transferAmt"));
                }
                
                request.setAttribute("bean", bean);
                
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(userDashboard.class.getName()).log(Level.SEVERE, null, ex);
            }
            super.doGet(request, response);
            dashboardDispatcher = request.getRequestDispatcher("userDashboard.jsp");
            dashboardDispatcher.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}