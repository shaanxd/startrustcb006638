/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "addBeneficiary", urlPatterns = {"/addBeneficiary"})
public class addBeneficiary extends pageBase {
    RequestDispatcher rd;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            super.doGet(request, response);
            rd = request.getRequestDispatcher("addBeneficiary.jsp");
            rd.forward(request, response);
        }
        else{
            response.sendRedirect("login");
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String userID = (String)session.getAttribute("userID");
        addBeneficiaryBean bean;
        String accType = request.getParameter("beneficiaryType");
        if(accType.equals("Startrust")||accType.equals("Mobile")){
            bean = new addBeneficiaryBean(accType,request.getParameter("beneficiaryName"),request.getParameter("beneficiaryNumber"));
        }
        else{
            bean = new addBeneficiaryBean(accType,request.getParameter("beneficiaryName"),request.getParameter("beneficiaryNumber"),request.getParameter("beneficiaryBank"),request.getParameter("beneficiaryBranch"));
        }
        try{
            PreparedStatement ps;
            ResultSet rs;
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            ps = con.prepareStatement("select * from beneficiaryprofile where userID=? and benNumber=?");
            ps.setString(1, userID);
            ps.setString(2, bean.getBenNum());
            rs = ps.executeQuery();
            if(rs!=null&&rs.next()){
                request.setAttribute("yesAcc", true);
                this.doGet(request, response);
            }
            else{
                if(accType.equals("Startrust")){
                    ps = con.prepareStatement("select * from bankaccount where accountNumber=?");
                    ps.setString(1, bean.getBenNum());
                    rs = ps.executeQuery();
                    if(rs!=null&&rs.next()){
                        ps = con.prepareStatement("insert into beneficiaryprofile (userID,benType,benName,benNumber,benBank,benBranch) values (?,?,?,?,?,?)");
                        ps.setString(1, userID);
                        ps.setString(2, accType);
                        ps.setString(3, bean.getBenName());
                        ps.setString(4, bean.getBenNum());
                        ps.setString(5, "Startrust");
                        ps.setString(6, rs.getString("branchName"));
                    }
                    else{
                        request.setAttribute("noAcc", true);
                        this.doGet(request, response);
                    }
                }
                else if(accType.equals("Mobile")){
                        ps = con.prepareStatement("insert into beneficiaryprofile (userID,benType,benName,benNumber) values (?,?,?,?)");
                        ps.setString(1, userID);
                        ps.setString(2, accType);
                        ps.setString(3, bean.getBenName());
                        ps.setString(4, bean.getBenNum());
                }
                else{
                    ps = con.prepareStatement("insert into beneficiaryprofile (userID,benType,benName,benNumber,benBank,benBranch) values(?,?,?,?,?,?)");
                    ps.setString(1, userID);
                    ps.setString(2, accType);
                    ps.setString(3, bean.getBenName());
                    ps.setString(4, bean.getBenNum());
                    ps.setString(5, bean.getBenBank());
                    ps.setString(6, bean.getBenBranch());
                }
                ps.executeUpdate();
                request.setAttribute("successMsg", true);
                this.doGet(request, response);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(addBeneficiary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
