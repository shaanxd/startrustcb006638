/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.*;
import utilities.databaseConnection;
import utilities.passwordEncryption;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {
    
    RequestDispatcher loginDispatcher;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        loginDispatcher = request.getRequestDispatcher("loginPage.jsp");
        loginDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            loginBean bean = new loginBean(request.getParameter("username"),passwordEncryption.encrypt(request.getParameter("password")));
            
            PreparedStatement ps2;
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("select * from useraccount where userName=? and userPassword=?");
            ps.setString(1, bean.getUsername());
            ps.setString(2, bean.getPassword());
            ResultSet rs = ps.executeQuery();
            
            if(rs!=null && rs.next())
            {
                session.setAttribute("userID", rs.getString("userID"));
                session.setAttribute("userType", rs.getString("userType"));
                if(rs.getString("userType").equals("customer")){
                    activityLogBean bean2 = new activityLogBean("Login");
                    bean2.setActivityDescription();

                    ps2 = con.prepareStatement("insert into activitylog (userID,activityType,activityDescription,activityDate) VALUES (?,?,?,?)");
                    ps2.setString(1, rs.getString("userID"));
                    ps2.setString(2, bean2.getActivityType());
                    ps2.setString(3, bean2.getActivityDescription());
                    ps2.setString(4, dateFormat.format(date));
                    
                    ps2.executeUpdate();
                }
                switch(rs.getString("userType")){
                    case "customer": response.sendRedirect("userDashboard"); break;
                    case "admin": response.sendRedirect("adminDashboard"); break;
                    case "manager": response.sendRedirect("managerDashboard"); break;
                }
            }
            else
            {
                request.setAttribute("username", bean.getUsername());
                request.setAttribute("errorMessage",true);
                loginDispatcher = request.getRequestDispatcher("loginPage.jsp");
                loginDispatcher.forward(request, response);
            }
        } catch (ClassNotFoundException | SQLException | NoSuchAlgorithmException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
