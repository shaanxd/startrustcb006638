/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.viewUsersBean;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "viewUsers", urlPatterns = {"/viewUsers"})
public class viewUsers extends pageBase {

    RequestDispatcher rd;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("userID")!=null){
            viewUsersBean bean = new viewUsersBean();
            PreparedStatement ps; ResultSet rs;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");
                Connection con = databaseConnection.createConnection();
                ps = con.prepareStatement("SELECT * FROM useraccount where userType=?");
                ps.setString(1, "customer");
                rs = ps.executeQuery();
                while(rs!=null&&rs.next()){
                    bean.addUser(rs.getString("userID"), rs.getString("firstName"), rs.getString("lastName"), rs.getString("userEmail"), rs.getString("lastLogin"));
                }
                request.setAttribute("bean", bean);
                
                super.doGet(request, response);
                rd = request.getRequestDispatcher("viewUsers.jsp");
                rd.forward(request, response);
            } catch (ClassNotFoundException | SQLException ex) {
                Logger.getLogger(viewPaymentProfiles.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            response.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
}
