/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.activityLogBean;
import utilities.*;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "logout", urlPatterns = {"/logout"})
public class logout extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        String userID = (String) session.getAttribute("userID");
        String userType = (String) session.getAttribute("userType");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        try {            
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE useraccount SET lastLogin=? where userID=?");
            ps.setString(1, dateFormat.format(date));
            ps.setString(2,userID);
            ps.executeUpdate();
            
            if(userType.equals("customer")){
                    activityLogBean bean2 = new activityLogBean("Logout");
                    bean2.setActivityDescription();

                    ps= con.prepareStatement("insert into activitylog (userID,activityType,activityDescription,activityDate) VALUES (?,?,?,?)");
                    ps.setString(1, userID);
                    ps.setString(2, bean2.getActivityType());
                    ps.setString(3, bean2.getActivityDescription());
                    ps.setString(4, dateFormat.format(date));
                    
                    ps.executeUpdate();
                }
            
            session.invalidate();
            response.sendRedirect("login");
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
