/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utilities.databaseConnection;

/**
 *
 * @author Shaan
 */
@WebServlet(name = "pageBase", urlPatterns = {"/pageBase"})
public class pageBase extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            String userID = (String) session.getAttribute("userID");
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = databaseConnection.createConnection();
            PreparedStatement ps = con.prepareStatement("select * from useraccount where userID=?");
            ps.setString(1,userID);
            ResultSet rs = ps.executeQuery();
            
            if(rs!=null && rs.next())
            {
                request.setAttribute("firstName", rs.getString("firstName"));
                request.setAttribute("userType",rs.getString("userType"));
                request.setAttribute("lastLogin", rs.getString("lastLogin"));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
}
