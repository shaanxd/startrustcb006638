<%-- 
    Document   : viewStandingOrders
    Created on : Jan 19, 2018, 10:00:39 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/viewStandingOrders.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>View Standing Orders - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>View Orders</h1>
                    <div class="cardListDiv">
                        <c:forEach var="order" items="${bean.getOrderList()}">
                            <form action="viewStandingOrders" method="POST">
                                <div class="card">                                            
                                    <label id="accName"><c:out value="${order.getDestType()}"/></label><br>        
                                    From : <label><c:out value="${order.getSourceNum()}"/></label><br>                    
                                    Beneficiary : <label><c:out value="${order.getDestNum()}"/></label><br>
                                    Duration : <label><c:out value="${order.getOrderDuration()} ${order.getDurationType()}"/></label><br> 
                                    Amount : <label><c:out value="Rs. ${order.getOrderAmount()}"/></label><br> 
                                    Starting : <label><c:out value="${order.getStartDate()}"/></label><br>   
                                    <input type="hidden" name="orderID" value="${order.getOrderID()}">
                                    <input type="submit" value="Cancel" id="cancelBtn">
                                </div>
                            </form>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
