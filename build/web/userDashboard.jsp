<%-- 
    Document   : userDashboard
    Created on : Jan 6, 2018, 12:46:27 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/userDashboard.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Dashboard - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Dashboard</h1>
                    <div class="divContainer">
                        <div class="row topDiv">
                            <div class="firstDiv col-sm-5">
                                <h4>Transactions <small>within the last 7 days</small></h4>
                                <div class="transactionDiv">
                                    <c:forEach var="transaction" items="${bean.getTransactionList()}">
                                        <div class="singleDiv">                                
                                            <label id="accountBalance"><c:out value="Rs.${transaction.getTransferAmt()}"/></label><br>   
                                            Source : <label><c:out value="${transaction.getSourceNum()}"/></label><br>                    
                                            Beneficiary : <label><c:out value="${transaction.getDestinationNum()}"/></label><br>                                   
                                            Date : <label><c:out value="${transaction.getTransferDate()}"/></label><br>
                                        </div>
                                    </c:forEach>      
                                </div>
                            </div>
                            <div class="col-sm-7 secondDiv">
                                <h4>My Accounts</h4>
                                <div class="accountsDiv">
                                    <c:forEach var="account" items="${bean.getAccountList()}">
                                        <div class="singleDiv">                                
                                            <label id="accountBalance"><c:out value="Rs.${account.getAccountBalance()}"/></label><br>
                                            <label id="accountName"><c:out value="${account.getName()}"/></label><br>                    
                                            A/C Num :<label><c:out value="${account.getAccountNumber()}"/></label><br>    
                                        </div>
                                    </c:forEach>                           
                                </div>
                                <h4>Messages</h4>
                                <div class="inboxDiv">
                                    <c:forEach var="message" items="${bean.getInboxList()}">
                                        <div class="singleDiv">                    
                                            <label id="accountBalance"><c:out value="${message.getSubject()}"/></label><br>                        
                                            <label><c:out value="${message.getOtherUser()}"/></label><br>                       
                                            <label><c:out value="${message.getDate()}"/></label><br>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
