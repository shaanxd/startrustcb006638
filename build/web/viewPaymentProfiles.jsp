<%-- 
    Document   : viewPaymentProfiles
    Created on : Jan 19, 2018, 11:15:57 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/viewPaymentProfiles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>View Payment Profiles - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Payment Profiles</h1>
                    <div class="cardListDiv">
                        <c:forEach var="payment" items="${bean.getPaymentList()}">
                            <div class="input-group input-group-lg">
                                <c:if test = "${payment.getProfileType() == 'Electricity'}">
                                    <span class="input-group-addon electricity"><span class="glyphicon glyphicon-flash"></span></span>
                                </c:if>
                                <c:if test = "${payment.getProfileType() == 'Insurance'}">
                                    <span class="input-group-addon insurance"><span class="glyphicon glyphicon-usd"></span></span>
                                </c:if>
                                <c:if test = "${payment.getProfileType() == 'Water'}">
                                    <span class="input-group-addon water"><span class="glyphicon glyphicon-tint"></span></span>
                                </c:if>
                                <c:if test = "${payment.getProfileType() == 'Cable TV'}">
                                    <span class="input-group-addon tv"><span class="glyphicon glyphicon-blackboard"></span></span>
                                </c:if>
                                <c:if test = "${payment.getProfileType() == 'Telephone'}">
                                    <span class="input-group-addon phone"><span class="glyphicon glyphicon-phone-alt"></span></span>
                                </c:if>
                                <c:if test = "${payment.getProfileType() == 'Internet'}">
                                    <span class="input-group-addon internet"><span class="glyphicon glyphicon-globe"></span></span>
                                </c:if>
                                <div class="card">                        
                                    <label id="paymentTypeLabel"><c:out value="${payment.getProfileType()}"/></label><br>                    
                                    Alias : <label><c:out value="${payment.getProfileName()}"/></label><br>                       
                                    A/C Num : <label><c:out value="${payment.getAccountNum()}"/></label><br>
                                </div>
                            </div>
                            <hr>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
