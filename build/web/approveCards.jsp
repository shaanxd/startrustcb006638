<%-- 
    Document   : approveCards
    Created on : Jan 16, 2018, 2:00:29 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/approvalPage.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Approve Cards - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Approve Cards</h1>
                    <p>Please select an account from the given list<br>
                    <div class="requestDiv">
                        <div class="col-sm-5 requestListDiv">
                            <div class="firstDiv">
                                <c:forEach var="request" items="${requestList.getRequestList()}">
                                    <div class="request">
                                        <label id="requestLabel" style="display:none"><c:out value="${request.getRequestID()}"/></label>      
                                        <label id="typeLabel"><c:out value="${request.getCardType()}"/></label><br>                       
                                        <label id="userLabel"><c:out value="${request.getUsername()}"/></label><br>                           
                                        Date: <label id="dateLabel"><c:out value="${request.getRequestDate()}"/></label><br> 
                                        Credit Limit: <label id="creditLabel"><c:out value="${request.getCreditLimit()}"/></label> 
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-sm-7 selectedRequestDiv" style="display:none">
                            <div class="secondDiv">
                                <form action="approveCards" method="POST">
                                    <div class="form-group" style="display:none">
                                        <label for="">Request ID</label>
                                        <input type="text" class="txtBox" name="requestID" id="requestID"readonly>    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Username</label>
                                        <input type="text" class="txtBox" name="username" id="username" readonly>    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Card Type</label>
                                        <input type="text" class="txtBox" name="cardType" id="cardType" readonly>    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Credit Limit</label>
                                        <input type="text" class="txtBox" name="creditLimit" id="creditLimit" readonly>    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Expiry Date</label>
                                        <input type="date" class="inputBox" name="expiryDate" id="expiryDate" required>    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Card Number</label>
                                        <input type="text" class="inputBox" name="cardNumber" id="cardNumber" required>    
                                    </div>
                                    <div class="btn-group-justified btn-group form-group" data-toggle="buttons">                                    
                                        <label class="radioBtn btn btn-default"><input type="radio" name="approvalType" id="approve" value="Approve" required>Approve</label>                                    
                                        <label  class="radioBtn btn btn-default"><input type="radio" name="approvalType" id="reject"  value="Reject" required>Reject</label>                                        
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Submit" id="submitBtn">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="static/js/approveCards.js"></script>
    </body>
</html>
