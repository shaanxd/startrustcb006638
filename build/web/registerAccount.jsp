<%-- 
    Document   : registerAccount
    Created on : Dec 24, 2017, 4:33:11 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/registerAccount.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Create Account - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form id="registerForm" action="registerAccount" method="POST">
                    <div class="formContainer">                        
                        <h1>Create Portal Account</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p>
                        <c:if test="${not empty successMessage}">
                            <div class="form-group alert alert-success" id="successMessage">
                                <strong>Success!</strong> You have successfully added the account to the system.
                            </div>
                        </c:if>
                        <h4 id="containerHeader">
                            Personal Details
                        </h4>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Username*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>                                    
                                    <input type ="text" class="inputBox" placeholder="Username" name="username" id="username" required>
                                </div> 
                            </div>
                            <div class="col-sm-8 form-group">
                                <label class="control-label">Email*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">@</span>
                                    <input type ="email" class="inputBox" placeholder="Email" name="emailAddress" id="emailAddress" required>
                                </div>
                            </div>
                        </div>
                        <c:if test="${not empty usernameError}">
                            <div class="form-group alert alert-danger" id="invalidUsername">
                                <strong>Error!</strong> The Username already exists in the system.
                            </div>
                        </c:if>
                        <c:if test="${not empty emailError}">
                            <div class="form-group alert alert-danger" id="invalidEmail">
                                <strong>Error!</strong> The Email already exists in the system.
                            </div>
                        </c:if>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">First Name*</label>
                                <input type ="text" class="inputBox" placeholder="First Name" name="firstName" required>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Middle Name</label>
                                <input type ="text" class="inputBox" placeholder="Middle Name" name="middleName">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Last Name*</label>
                                <input type ="text" class="inputBox" placeholder="Last Name" name="lastName" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">D.O.B*</label>
                                <input type ="date" class="inputBox" placeholder="Date Of Birth" name="dateOfBirth" required>                                
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">N.I.C*</label>                                
                                <div class="input-group">
                                    <input type ="text" class="inputBox" placeholder="N.I.C" name="identityNumber" required>
                                    <span class="input-group-addon">v</span>
                                </div>                                
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Passport I.D</label>
                                <input type ="text" class="inputBox" placeholder="Passport I.D" name="passportNumber">
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-sm-4 form-group">
                                <label>Gender*</label><br>
                                <label class="radio-inline"><input type="radio" name="gender" value="M" required> Male</label>
                                <label class="radio-inline"><input type="radio" name="gender" value="F" required> Female</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8 form-group">
                                <label class="control-label">Address*</label>
                                <input type ="text" class="inputBox" placeholder="Address" name="address" required>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">City*</label>
                                <input type ="text" class="inputBox" placeholder="City" name="city" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 form-group">
                                <label class="control-label">ZIP Code</label>
                                <input type ="text" class="inputBox" placeholder="ZIP Code" name="zipCode">
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Mobile No.*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
                                    <input type ="text" class="inputBox" placeholder="Mobile No." name="mobileNumber" required>
                                </div>
                            </div>
                            <div class="col-sm-4 form-group">
                                <label class="control-label">Home No.</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
                                    <input type ="text" class="inputBox" placeholder="Home No." name="homeNumber">
                                </div>
                            </div>
                        </div>
                        <hr id="horizontalLine">
                        <h4 id="containerHeader">
                            Account Details
                        </h4>
                        <div class="row">
                            <div class="col-sm-4 form-group">                 
                                <label class="control-label">Account Type*</label>
                                <select class="inputBox" name="accountType" required>
                                    <option disabled selected value>Select Type</option>
                                    <option value="Savings">Savings Account</option>
                                    <option value="Current">Current Account</option>
                                    <option value="Fixed Deposit">Fixed-Deposit Account</option>
                                </select>
                            </div>
                            <div class="col-sm-8 form-group">
                                <label class="control-label">Account No.</label>
                                <input type ="text" class="inputBox" placeholder="Account No." name="accountNumber" id="accountNumber" required>
                            </div>                               
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label class="control-label">Account Name</label>
                                <input type ="text" class="inputBox" placeholder="Account Name" name="accountName" id="accountName" required>
                            </div>                               
                        </div>
                        <c:if test="${not empty accountNumError}">
                            <div class="form-group alert alert-danger" id="invalidAccount">
                                <strong>Error!</strong> The Account already exists in the system.
                            </div>
                        </c:if>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Branch*</label>
                                <select class="inputBox" name="branchName" required>
                                    <option  disabled selected value>Select Branch</option>
                                    <option value="Wattala">Wattala</option>
                                    <option value="Kolpity">Kolpity</option>
                                    <option value="Dehiwala">Dehiwala</option>
                                    <option value="Kirulapone">Kirulapone</option>
                                </select>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Initial Deposit</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type ="number" class="inputBox" placeholder="Amount" name="initialAmount">
                                </div>
                            </div>  
                        </div>
                        <div class="row">
                            <div class="col-sm-6" form-group>
                                <br>
                                <input type ="submit" id="submitBtn" value="Submit">
                            </div>
                            <div class="col-sm-6" form-group>
                                <br>
                                <input type ="reset" id="resetBtn" value="Reset">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
