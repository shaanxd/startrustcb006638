<%-- 
    Document   : viewBeneficiaries
    Created on : Jan 20, 2018, 1:03:44 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/viewBeneficiaries.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>View Beneficiaries - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>View Beneficiaries</h1>
                    <div class="cardListDiv">
                        <c:forEach var="ben" items="${bean.getBenList()}">
                            <div class="card">                        
                                <input type="hidden" name="orderID" value="${ben.getBeneficiaryID()}">
                                <label id="accName"><c:out value="${ben.getAccountName()}"/></label><br>                    
                                Number : <label><c:out value="${ben.getAccountNumber()}"/></label><br>                       
                                <label><c:out value="${ben.getAccountType()} Beneficiary"/></label><br>            
                                Bank : <label><c:out value="${ben.getBenBank()}"/></label><br> 
                                Branch : <label><c:out value="${ben.getBenBranch()}"/></label><br> 
                            </div>
                        </c:forEach>                           
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
