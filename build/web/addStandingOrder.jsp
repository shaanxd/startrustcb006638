<%-- 
    Document   : addStandingOrder
    Created on : Dec 29, 2017, 11:40:11 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/addBSO.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Add Order - Startrust</title>
    </head>
    <body>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Add Standing Order</h1>
                    <p>Please note that the fields marked with * are <strong>Mandatory</strong></p>
                    <div class="formDiv">
                        <form id="myForm" action="addStandingOrder" method="POST">                           
                            <p>Please select the <strong>type</strong> of Standing Order to be added.</p>
                            <div class="form-group">
                                <label for="sourceAccount">Source Account:*</label>
                                <select class="inputBox" name="sourceAccount" id="sourceAccount" required>
                                    <option disabled selected value>Select Account</option>
                                    <c:forEach var="account" items="${bean.getAccountList()}">
                                        <option value="${account.getAccountNumber()}" id="${account.getAccountBalance()}">
                                            <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="orderType" value="beneficiary" checked required>Beneficiary Account</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="orderType" value="payment" required>Payment Account</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="orderType" value="creditCard" required>Credit Card</label>
                            </div>
                            <div class="beneficiary order">
                                <div class="form-group">
                                    <label for="beneficiaryProfile">Select Beneficiary:*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>    
                                        <select class="inputBox" name="beneficiaryProfile" id="beneficiaryProfile" required>
                                            <option disabled selected value>Select Account</option>
                                            <c:forEach var="account" items="${bean.getBenList()}">
                                                <option value="${account.getBeneficiaryID()}">
                                                    <c:out value="${account.getAccountName()} : ${account.getAccountNumber()} : ${account.getAccountType()}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="payment order" style="display:none">
                                <div class="form-group">
                                    <label for="paymentProfile">Select Payment:*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-list-alt"></i></span>   
                                        <select class="inputBox" name="paymentProfile" id="paymentProfile" required disabled>
                                            <option disabled selected value>Select Profile</option>
                                            <c:forEach var="profile" items="${bean.getProfileList()}">
                                                <option value="${profile.getProfileID()}">
                                                    <c:out value="${profile.getProfileName()} : ${profile.getProfileType()} : ${profile.getAccountNum()}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="creditCard order" style="display:none">
                                <div class="form-group">
                                    <label for="cardProfile">Select Card:*</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-credit-card"></i></span>
                                        <select class="inputBox" name="cardProfile" id="cardProfile" required disabled>
                                            <option disabled selected value>Select Card</option>
                                            <c:forEach var="profile" items="${bean.getCardList()}">
                                                <option value="${profile.getCardNumber()}">
                                                    <c:out value="${profile.getCardType()} : ${profile.getCardNumber()}"/>
                                                </option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="orderAmount">Payment Amount:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>                                    
                                    <input type="number" class="inputBox" name="orderAmount" id="orderAmount" placeholder="Enter Amount" required>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label for="startFrom">From:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <input type="date" class="inputBox" name="startFrom" id="startFrom" required>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label for="orderDuration">Payment Duration:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    <input type="number" class="inputBox" name="orderDuration" id="orderDuration" placeholder="Enter Duration" required>
                                </div>
                                <br>
                                <div class="btn-group-justified btn-group" data-toggle="buttons">                                    
                                    <label class="btn btn-custom active"><input type="radio" name="durationType" value="Day(s)" required checked>Days</label>                                    
                                    <label  class="btn btn-custom"><input type="radio" name="durationType" value="Week(s)" required>Weeks</label>                                    
                                    <label  class="btn btn-custom"><input type="radio" name="durationType" value="Month(s)" required>Months</label>                               
                                </div>
                            </div>                            
                            <div class="form-group alert alert-danger" id="amtError" style="display:none">
                                <strong>Error!</strong> The source account does not have sufficient funds.
                            </div>
                            <div class="form-group alert alert-danger" id="negError" style="display:none">
                                <strong>Error!</strong> The number cannot be negative.
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="static/js/addStandingOrder.js"></script>
    </body>
</html>
