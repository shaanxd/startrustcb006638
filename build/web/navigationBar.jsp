<%-- 
    Document   : customerNavigationBar
    Created on : Dec 14, 2017, 9:19:03 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="static/css/navigationBar.css">
        <script src="//code.jquery.com/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
        <title>Navigation Bar</title>
    </head>
    <body>
        <nav id="sidebar">
            <div id="buttonContainer">
                <button type="button" id="closeBtn" class="barButton">
                    <i class="glyphicon glyphicon-remove"></i>
                </button>
            </div>
            <div class ="sidebar-header">
                <h3>Startrust Bank</h3>
            </div>
            <ul class="list-unstyled components">
                <c:if test="${requestScope.userType=='customer'}">
                    <li class="active"><a href="userDashboard">Dashboard</a></li>
                    <li>
                        <a data-toggle="collapse" href="#accountsToggle" aria-expanded="false">Accounts</a>
                        <ul class="collapse list-unstyled" id="accountsToggle">
                            <li><a href="openAccount">Request Account</a></li>
                            <li><a href="myAccounts">My Accounts</a></li>
                            <li><a href="transactionHistory">Transactions History</a></li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#transferToggle" aria-expanded="false">Fund Transfers</a>
                        <ul class="collapse list-unstyled" id="transferToggle">
                            <li><a href="addBeneficiary">Add Beneficiary</a></li>
                            <li><a href="viewBeneficiaries">Beneficiary list</a></li>
                            <li><a href="transferFunds">Transfer Funds</a></li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#cardsToggle" aria-expanded="false">Credit Cards</a>
                        <ul class="collapse list-unstyled" id="cardsToggle">
                            <li><a href="requestCreditCard">Request Card</a></li>
                            <li><a href="viewCards">View Cards</a></li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#paymentToggle" aria-expanded="false">Payments</a>
                        <ul class="collapse list-unstyled" id="paymentToggle">
                            <li><a href="addPaymentProfile">Add Profile</a></li>
                            <li><a href="viewPaymentProfiles">View Profiles</a></li>
                            <li><a href="openPayment">Open Payments</a></li>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#orderToggle" aria-expanded="false">Standing Orders</a>
                        <ul class="collapse list-unstyled" id="orderToggle">
                            <li><a href="addStandingOrder">Add Standing Order</a></li>
                            <li><a href="viewStandingOrders">View Standing Orders</a></li>
                        </ul>
                    </li> 
                </c:if>
                <c:if test="${requestScope.userType=='admin'}">
                    <li class="active"><a href="adminDashboard">Dashboard</a></li>
                    <li>
                        <a data-toggle="collapse" href="#accountsToggle" aria-expanded="false">Accounts</a>
                        <ul class="collapse list-unstyled" id="accountsToggle">
                            <li><a href="viewUsers">View Users</a></li>
                            <li><a href="approveAccounts">Account Approvals</a></li>
                            <li><a href="activityLog">Activity Log</a></li>
                            <li><a href="registerAccount">Register Account</a></li>
                        </ul>
                    </li>
                    <li><a href="verifyInterest">Verify Interest</a></li>
                </c:if>
                <c:if test="${requestScope.userType=='manager'}">
                    <li class="active"><a href="managerDashboard">Dashboard</a></li>
                    <li><a href="approveCards">Card Approvals</a></li>
                </c:if>
                <li><a href="userMessages">Messages</a></li>
            </ul>
            <div id="profileContainer">
                <center>
                <div id="imageContainer">
                    <img id="profileImage" src="static/img/avatar.png">
                    <div id="profileHiddenButton">
                        <center><a id = "changeProfilePicBtn" href="manageProfile">Update</a></center>
                    </div>
                </div>
                <div id="detailsContainer">
                    Welcome, <c:out value="${requestScope.firstName}" ></c:out> <br>
                    Last Login: <c:out value="${requestScope.lastLogin}" ></c:out>
                </div>
                <a id = "logoutBtn" href="logout">Logout</a>
                </center>
            </div>
        </nav>
        <script src="static/js/navigationBar.js"></script>
    </body>
</html>
