<%-- 
    Document   : userMessages
    Created on : Jan 3, 2018, 9:20:15 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/userMessages.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Messages - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <div class="formContainer">
                    <h1>Messages</h1>
                    <div class="btn-group-justified btn-group" data-toggle="buttons">                                    
                        <label class="radioBtn btn btn-custom active" id="inbox"><input type="radio" name="messagesType" value="inbox" required checked>Inbox</label>                                    
                        <label  class="radioBtn btn btn-custom" id="sent"><input type="radio" name="messagesType" value="sent" required>Outbox</label>                                    
                        <label  class="radioBtn btn btn-custom" id="compose"><input type="radio" name="messagesType" value="compose" required>Send</label>                               
                    </div>
                    <br>
                    <div class="inboxMessagesDiv">
                        <div class="col-sm-5 messageListDiv">
                            <div class="singleDiv">
                                <c:forEach var="user" items="${userList.getOutboxList()}">
                                    <div class="inboxMessage">                              
                                        <label id="subjectLabel"><c:out value="${user.getSubject()}"/></label><br> 
                                        <label id="userLabel"><c:out value="${user.getOtherUser()}"/></label><br>                          
                                        <label id="dateLabel"><c:out value="${user.getDate()}"/></label>
                                        <label id="bodyLabel" style="display:none"><c:out value="${user.getBody()}"/></label> 
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-sm-7 messageDiv">
                            <div class="inboxSelectedMessage" style="display:none">
                                <label id="inboxUserLabel"></label><br>                               
                                <label id="inboxSubjectLabel"></label><br>                            
                                <label id="inboxDateLabel"></label><hr>                     
                                <label id="inboxBodyLabel"></label><hr>
                                <input id="replyBtn" type="button" value="Reply">
                            </div>                            
                        </div>
                    </div>
                    <div class="sentMessagesDiv" style="display:none">
                        <div class="col-sm-5 messageListDiv">
                            <div class="singleDiv">
                                <c:forEach var="user" items="${userList.getInboxList()}">
                                    <div class="outboxMessage">                              
                                        <label id="subjectLabel"><c:out value="${user.getSubject()}"/></label><br>  
                                        <label id="userLabel"><c:out value="${user.getOtherUser()}"/></label><br>                         
                                        <label id="dateLabel"><c:out value="${user.getDate()}"/></label>
                                        <label id="bodyLabel" style="display:none"><c:out value="${user.getBody()}"/></label>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-sm-7 messageDiv">
                            <div class="outboxSelectedMessage" style="display:none">                                
                                <label id="outboxUserLabel"></label><br>                          
                                <label id="outboxSubjectLabel"></label><br>                       
                                <label id="outboxDateLabel"></label><hr>                       
                                <label id="outboxBodyLabel"></label><hr>
                                <input id="replyBtn" type="button" value="Send">
                            </div>                       
                        </div>
                    </div>
                    <div class="composeMessagesDiv messagesDiv" style="display:none">
                        <form action="userMessages" method="post" class="composeForm">
                            <br>
                            <div class="form-group">
                                <label for="recipientName">Recipient Name:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <select class="inputBox" name="recipientName" id="recipientName" required>
                                        <option disabled selected value>Choose Recipient</option>
                                        <c:forEach var="user" items="${userList.getUserList()}">
                                            <option value="${user}">
                                                <c:out value="${user}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="messageSubject">Subject*</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="text" class="inputBox" name="messageSubject" id="messageSubject" placeholder="Enter Subject" required>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="messageBody">Message*</label>
                                <textarea class="inputBox" name="messageBody" id="messageBody" placeholder="Enter Message" required></textarea>
                                <span id="remain">205</span> characters remaining
                            </div>
                            <div class="row">
                                <div class="col-sm-12">                                    
                                    <input type ="submit" id="submitBtn" value="Send">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="static/js/userMessages.js"></script>
    </body>
</html>
