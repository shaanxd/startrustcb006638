<%-- 
    Document   : Login
    Created on : Dec 11, 2017, 10:50:23 AM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="static/css/loginPage.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Login - Startrust</title>
    </head>
    <body>
        <div class ="CenterContainer">
            <div class="FormContainer">
                <form action="login" method="post">
                    <h1 id="welcomeText">Welcome to Startrust Banking Portal</h1>
                    <h3 id="enterText">
                        Please enter User ID and Password to continue.If you do not have an account
                        please <a id="registrationLink" href="static/file/Startrust_Form.docx">Click here.</a>
                    </h3>
                    <div class="form-group">
                        <label id="formLabel">User ID :</label>
                        <input required name="username" id ="formInput" type="text" placeholder="Enter User ID" value="${username}">
                    </div>
                    <div class="form-group">
                        <label id="formLabel">Password:</label>
                        <input required name="password" id ="formInput" type="password" placeholder="Enter Password">
                    </div>
                    <c:if test="${not empty errorMessage}">
                        <div class="form-group alert alert-danger" id="passwordError">
                            <center><strong>Wrong Credentials!</strong> Please enter valid credentials.</center>
                        </div>
                    </c:if>                    
                    <div class="form-group">
                        <input type="submit" id="submitBtn" value="Login">
                    </div>
                </form>
            </div>
            <div class="LinksContainer">
                <a id="helpLinks" href="resetPassword">Forgot Password?</a><br>
                <a id="helpLinks" href="">Help & Support</a>&emsp;
                <a id="helpLinks" href="">Contact</a>
            </div>
        </div>
    </body>
</html>
