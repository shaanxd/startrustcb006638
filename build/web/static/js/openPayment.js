/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('input[name="paymentType"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);                    
        $(".payment").not(targetBox).hide();
        $(targetBox).slideDown();
        if (inputValue===("Bill")){
            $(".Card :input").prop("disabled", true);
            $(".Bill :input").prop("disabled", false);
        }
        else if (inputValue===("Card")){
            $(".Bill :input").prop("disabled", true);                        
            $(".Card :input").prop("disabled", false);
        }
    });
    $('#myForm').submit(function() {
        var paymentAmount = parseFloat($('#paymentAmount').val());
        var availableAmount = parseFloat($('#sourceAccount option:selected').attr('id'));
        if (paymentAmount<0) {
            $('#transferError').fadeIn();
            $('#amtError').hide();
            return false;
        }
        else if(paymentAmount > availableAmount){
            $('#transferError').hide();
            $('#amtError').fadeIn();
            return false;
        }
        else{
            return true;
        }
    });
});


