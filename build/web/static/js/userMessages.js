/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('.radioBtn').click(function(){
        var inputValue = $(this).attr('id');
        if(inputValue==='inbox'){
            $('.inboxMessagesDiv').fadeIn();
            $('.sentMessagesDiv,.composeMessagesDiv').hide();
        }
        else if(inputValue==='sent'){
            $('.sentMessagesDiv').fadeIn();
            $('.inboxMessagesDiv,.composeMessagesDiv').hide();
        }
        else{
            $('.composeMessagesDiv').fadeIn();
            $('.inboxMessagesDiv,.sentMessagesDiv').hide();
        }
        $('.inboxSelectedMessage,.outboxSelectedMessage').hide();
        $('.inboxMessage,.outboxMessage').removeClass('active');
    });
    $('.inboxMessage').on('click',function(){
        $(".inboxSelectedMessage").hide();
        $(this).addClass('active').siblings().removeClass('active');
        var username = $(this).children("#userLabel").text();
        var subject = $(this).children("#subjectLabel").text();
        var body = $(this).children("#bodyLabel").text();                    
        var date = $(this).children("#dateLabel").text();
        $("#inboxUserLabel").text(username);
        $("#inboxSubjectLabel").text(subject);
        $("#inboxDateLabel").text(date);
        $("#inboxBodyLabel").text(body);
        $(".inboxSelectedMessage").fadeIn();
    });
    $('.outboxMessage').on('click',function(){
        $(".outboxSelectedMessage").hide();
        $(this).addClass('active').siblings().removeClass('active');
        var username = $(this).children("#userLabel").text();
        var subject = $(this).children("#subjectLabel").text();
        var body = $(this).children("#bodyLabel").text();                    
        var date = $(this).children("#dateLabel").text();
        $("#outboxUserLabel").text(username);
        $("#outboxSubjectLabel").text(subject);
        $("#outboxDateLabel").text(date);
        $("#outboxBodyLabel").text(body);
        $(".outboxSelectedMessage").fadeIn();
    });
    $(':button[id="replyBtn"]').on('click',function(){
        var selectedDiv = $(this).parent().attr('class');
        var x;
        if(selectedDiv==="inboxSelectedMessage"){
            x=$('#inboxUserLabel').text();
        }
        else{
            x=$('#outboxUserLabel').text()
        }
        $('#recipientName').val(x);
        $('.composeMessagesDiv').fadeIn();
        $('.inboxMessagesDiv,.sentMessagesDiv').hide();
        $('.inboxSelectedMessage,.outboxSelectedMessage').hide();
        $('.inboxMessage,.outboxMessage').removeClass('active');
        $('.radioBtn[id="compose"]').addClass('active').siblings().removeClass('active');
    });
    var maxchars = 205;
    $('textarea').keyup(function () {
        var tlength = $(this).val().length;
        $(this).val($(this).val().substring(0, maxchars));
        var tlength = $(this).val().length;
        remain = maxchars - parseInt(tlength);
        $('#remain').text(remain);
    });   
});


