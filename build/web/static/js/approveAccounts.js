/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');

        $('.collapse.in').toggleClass('in');

        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    $('.request').on('click',function(){
        $(".selectedRequestDiv").hide();
        $(this).addClass('active').siblings().removeClass('active');
        var user = $(this).children("#userLabel").text();
        var date = $(this).children("#dateLabel").text();
        var type = $(this).children("#typeLabel").text();                    
        var request = $(this).children("#requestLabel").text();   
        var branch = $(this).children("#branchLabel").text();
        $("#requestID").val(request);
        $("#username").val(user);
        $("#accType").val(type);
        $("#reqDate").val(date);
        $("input:radio").removeAttr("checked");
        $(".radioBtn").removeClass('active');
        $(".selectedRequestDiv").fadeIn();
    });
    $('#approve').change(function () {
        if($(this).is(':checked')) {
            $('#accountNumber').attr('required');
        } else {
            $('#accountNumber').removeAttr('required');
        }
    });
    $('#reject').change(function () {
        if($(this).is(':checked')) {
            $('#accountNumber').removeAttr('required');
        }
    });
});

