<%-- 
    Document   : requestCreditCard
    Created on : Dec 27, 2017, 2:17:42 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Credit Cards - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form id="myForm" action="requestCreditCard" method="post">
                    <div class="formContainer">
                        <h1>Request Credit Card</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p><br>
                        <div class="divContainer">
                            <div class="form-group">
                                <label for="cardType">Card Type:*</label>
                                <select class="inputBox" name="cardType" id="cardType" required>
                                    <option disabled selected value>Select Type</option>
                                    <option value="Gold">Gold</option>
                                    <option value="Platinum">Platinum</option>
                                    <option value="Diamond">Diamond</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sourceAccount">Fee Source:*</label>
                                <select class="inputBox" name="sourceAccount" id="sourceAccount" required>
                                    <option disabled selected value>Select Account</option>
                                    <c:forEach var="account" items="${accountList.getAccountList()}">
                                        <option value="${account.getAccountID()}" id="${account.getAccountBalance()}">
                                            <c:out value="${account.getAccountNumber()} : ${account.getName()}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </div>                           
                            <div class="form-group">
                                <label for="creditLimit">Credit Limit:*</label>
                                <div class="input-group">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" class="inputBox" name="creditLimit" id="creditLimit" placeholder="Enter Limit" required>
                                </div>
                            </div>
                            <div class="form-group alert alert-danger" id="wrongAmount" style="display:none">
                                <strong>Error!</strong> This account does not have sufficient funds.
                            </div>
                            <c:if test="${not empty successMessage}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> You have successfully requested a credit card.
                                </div>
                            </c:if>
                            <p> Please be informed that a fee of <strong> Rs.200/= </strong>will be deducted from the 
                                selected <strong>Source Account</strong> should the request be accepted.</p>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Request Card">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="static/js/requestCreditCard.js"></script>
    </body>
</html>
