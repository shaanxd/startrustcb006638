<%-- 
    Document   : resetPassword
    Created on : Jan 22, 2018, 9:20:50 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="static/css/loginPage.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Reset Password - Startrust</title>
    </head>
    <body>
        <div class ="CenterContainer">
            <div class="FormContainer">
                <form action="resetPassword" method="POST">
                    <h1 id="welcomeText">Reset Password</h1>
                    <h3 id="enterText">
                        Please enter your email address to reset your password. You will recieve a temperory password which
                        will enable you to login to the system.
                    </h3>
                    <div class="form-group">
                        <label id="formLabel">User Email :</label>
                        <input required name="userEmail" id ="formInput" type="text" placeholder="Enter User Email">
                    </div>
                    <c:if test="${not empty errorMessage}">
                        <div class="form-group alert alert-danger" id="passwordError">
                            <center><strong>Error! </strong>User does not exist!</center>
                        </div>
                    </c:if>                    
                    <div class="form-group">
                        <input type="submit" id="submitBtn" value="Reset">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
