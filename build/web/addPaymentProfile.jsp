<%-- 
    Document   : addPaymentProfile
    Created on : Dec 26, 2017, 1:10:01 PM
    Author     : Shaan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/css/pageLayout.css">
        <link rel="stylesheet" href="static/css/formLayout.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
        <title>Payment - Startrust</title>
    </head>
    <body>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.setHeader("Pragma", "no-cache");
        %>
        <div class="wrapper">
            <jsp:include page="navigationBar.jsp"/>  
            <div id="content">
                <button type="button" id="sidebarCollapse">
                    <i class="glyphicon glyphicon-menu-hamburger"></i>
                </button>
                <form action="addPaymentProfile" method="POST">
                    <div class="formContainer">
                        <h1>Add Payment Profile</h1>
                        <p>Please note that the fields marked with * are <strong>Mandatory</strong></p>
                        <br>
                        <div class="divContainer">
                            <div class="form-group">
                                <label for="profileName">Profile Name:*</label>
                                <input type="text" class="inputBox" name="profileName" id="profileName" placeholder="Enter Name" required>
                            </div>
                            <div class="form-group">
                                <label for="accountNo">Account No:*</label>
                                <input type="text" class="inputBox" name="accountNo" id="accountNo" placeholder="Ref Account No" required>
                            </div>
                            <div class="form-group">
                                <label for="profileType">Profile Type:*</label>
                                <select class="inputBox" name="profileType" id="profileType" required>
                                    <option  disabled selected value>Select Type</option>
                                    <option value="Electricity">Electricity</option>
                                    <option value="Water">Water</option>
                                    <option value="Cable TV">Cable TV</option>
                                    <option value="Telephone">Telephone</option>
                                    <option value="Internet">Internet</option>
                                    <option value="Insurance">Insurance</option>
                                </select>
                            </div>
                            <br>
                            <c:if test="${not empty successMessage}">
                                <div class="form-group alert alert-success" id="successMessage">
                                    <strong>Success!</strong> You have successfully requested a credit card.
                                </div>
                            </c:if>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="submit" id="submitBtn" value="Add Profile">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
            $(document).ready(function () {

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    
                    $('.collapse.in').toggleClass('in');
                
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
                
            });
        </script>
    </body>
</html>
